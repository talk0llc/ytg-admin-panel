// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // apiBaseUrl: 'https://shrouded-ridge-65941.herokuapp.com/api',
  // apiBaseUrl: 'http://localhost:3000/api',
  // apiBaseUrl: 'http://209.97.139.199:3000/api',
  apiBaseUrl: 'https://ytg.eco/api',
  // mainIpUrl: 'http://209.97.139.199',
  mainIpUrl: 'https://ytg.eco',
  // mainIpUrl: 'http://localhost:3000',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
