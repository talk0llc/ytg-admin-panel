import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ProgramsService} from '../../programs/programs.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-create-program',
  templateUrl: './create-program.component.html',
  styleUrls: ['./create-program.component.css']
})
export class CreateProgramComponent implements OnInit {
  public ctrl = this;
  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
  };
  public pinned = true;
  public is_active = false;
  public loading = false;
  public segments = [
    { color: 'Red', code: '#ff5a61' },
    { color: 'Blue', code: '#04adc7' },
    { color: 'Orange', code: '#fea601' },
    { color: 'Purple', code: '#8b199b' },
  ];
  public selectedFiles = [];
  public formData = new FormData();
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private programService: ProgramsService,
  ) { }

  ngOnInit() {
    this.buildForm();
  }
  get title() {
    return this.form.get('name');
  }

  get description() {
    return this.form.get('description');
  }
  get tag() {
    return this.form.get('tag');
  }

  get segment() {
    return this.form.get('segment');
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  create() {
    this.loading = true;
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      name: this.form.value.name,
      description: this.form.value.description,
      tag: this.form.value.tag,
      segment: this.form.value.segment,
      pinned: this.pinned,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.programService
      .create(this.formData)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","Program Created Successfully", "success")
            .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.router.navigate(['programs']);
          })
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }


  buildForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      description: [''],
      tags: [''],
      segment: [this.segments],
    });
  }
}
