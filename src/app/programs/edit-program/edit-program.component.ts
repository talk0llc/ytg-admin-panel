import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ProgramsService} from '../../programs/programs.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import { environment } from '../../../environments/environment';
declare var $;
@Component({
  selector: 'app-edit-program',
  templateUrl: './edit-program.component.html',
  styleUrls: ['./edit-program.component.css']
})
export class EditProgramComponent implements OnInit {
  public programId = this.route.snapshot.params.id;
  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
  };
  public pinned = true;
  public name = '';
  public description = '';
  public tags = '';
  public location = '';
  public segment = '';
  public segments = [
    { color: 'Red', code: '#ff5a61' },
    { color: 'Blue', code: '#04adc7' },
    { color: 'Orange', code: '#fea601' },
    { color: 'Purple', code: '#8b199b' },
  ];
  public sections = ['Text', 'File'];
  public dueDate: Date;
  public is_active = false;
  public loading = false;
  public selectedFiles = [];
  public imagesUrl = [];
  public extraSections = [];
  public mainIpUrl = environment.mainIpUrl;
  public editorConfig2: AngularEditorConfig = {
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: `/app/images/uploads`, // if needed
  };
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  public sec = '';
  public formData = new FormData();
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private programService: ProgramsService,
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getOne(this.programId);
  }
  get section() {
    return this.form.get('section');
  }

  urls1 = [];
  urls2 = [];
  onFileChanged(event, no) {
    this.selectedFiles = event.target.files;
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          if (no === 1) {
            this.urls1.push(event.target.result);
          } else {
            this.urls2.push(event.target.result);
          }
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  update() {
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      title: this.form.value.name,
      description: this.form.value.description,
      tags: this.form.value.tags,
      segment: this.form.value.segment,
      pinned: this.pinned,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.loading = true;
    this.programService
      .update(this.formData, this.programId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.formData = new FormData();
          this.router.navigate(['programs']);
        },
        (error: Response) => {
          this.loading = false;
          this.formData = new FormData();
        }
      );
  }

  getOne(id) {
    this.loading = true;
    this.urls1 = [];
    this.programService
      .getOne(id)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.name = result.name;
          this.description = result.description;
          this.tags = result.tags;
          this.segment = result.segment;
          this.location = result.location;
          this.dueDate = new Date(result.dueDate);
          this.pinned = result.pinned;
          this.is_active = result.is_active;
          this.extraSections = result.extraSections;
          for (let i = 0; i < result.images.length; i += 1) {
            this.imagesUrl.push(`${ environment.mainIpUrl }/${ result.images[i] }`);
          }
          this.urls1 = this.imagesUrl;
          this.imagesUrl = [];
          this.buildForm();
        },
        (error: Response) => {
          this.loading = false;
          this.imagesUrl = [];
        }
      );
  }

  updateSections() {
    const data = {
      extraSections: {
        // images: this.urls,
        description: this.form.value.extraDescription
      }
    };
    this.formData.append('data', JSON.stringify(data));
    this.loading = true;
    this.programService
      .update(this.formData, this.programId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.urls2 = [];
          this.form.controls['extraDescription'].setValue('');
          this.formData = new FormData();
          this.getOne(this.programId);
          this.reset();
        },
        (error: Response) => {
          this.loading = false;
          this.formData = new FormData();
          this.getOne(this.programId);
          this.reset();
        }
      );
  }

  onSelectType(type) {
    console.log(type.target)
    const description = document.getElementById('description');
    const file = document.getElementById('fileupload');
    const saveButton = document.getElementById('saveButton');
    if (type.value === 'Text') {
      description.removeAttribute('hidden');
      file.setAttribute('hidden', 'hidden');
    } else {
      file.removeAttribute('hidden');
      description.setAttribute('hidden', 'hidden');
    }
    saveButton.removeAttribute('disabled');
  }

  deleteSection(sectionId) {
    this.loading = true;
    this.programService
      .deleteSection(this.programId, sectionId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.getOne(this.programId);
        },
        (error: Response) => {
          this.loading = false;
          this.getOne(this.programId);
        }
      );
  }
  reset() {
    const description = document.getElementById('description');
    const file = document.getElementById('fileupload');
    const saveButton = document.getElementById('saveButton');
    file.setAttribute('hidden', 'hidden');
    description.setAttribute('hidden', 'hidden');
    saveButton.setAttribute('disabled', 'disabled');
  }

  buildForm() {
    this.form = this.fb.group({
      name: [this.name, Validators.required],
      description: [this.description, Validators.required],
      tags: [this.tags],
      segment: [this.segment],
      section: [this.sections],
      is_active: [this.is_active],
      pinned: [this.pinned],
      extraDescription: ['']
    });
  }

}
