import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AUTH_TOKEN, USER_ID} from '../constants';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgramsService {
  apiUrl = environment.apiBaseUrl;
  private userId: string = null;
  private _isAuthenticated = new BehaviorSubject(false);
  constructor(private http: HttpClient) { }

  create(data) {
    return this.http.post(`${this.apiUrl}/programs`, data, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } });
  }

  update(data, id) {
    return this.http.put( `${ this.apiUrl }/programs/${ id }`, data, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  getOne(id) {
    return this.http.get( `${ this.apiUrl }/programs/${ id }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  getAll(query) {
    return this.http.get( `${ this.apiUrl }/programs?${ query }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  delete(id) {
    return this.http.delete( `${ this.apiUrl }/programs/${ id }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  addQuestion(id, data) {
    return this.http.put( `${ this.apiUrl }/programs/${ id }/questions`, data,{ headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  getQuestions(id) {
    return this.http.get( `${ this.apiUrl }/programs/${ id }/questions`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  deleteQuestion(id, questionId) {
    return this.http.delete( `${ this.apiUrl }/programs/${ id }/questions/${ questionId }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  deleteSection(id, sectionId) {
    return this.http.delete( `${ this.apiUrl }/programs/${ id }/sections/${ sectionId }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }
}
