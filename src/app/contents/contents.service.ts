import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AUTH_TOKEN, USER_ID} from '../constants';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContentsService {
  apiUrl = environment.apiBaseUrl;
  private userId: string = null;
  private _isAuthenticated = new BehaviorSubject(false);
  constructor(private http: HttpClient) { }

  uploadSliderImages(data) {
    return this.http.post(`${this.apiUrl}/upload/slider`, data, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } });
  }

  uploadLogoImage(data) {
    return this.http.post(`${this.apiUrl}/upload/logos`, data, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } });
  }

  getSliders() {
    return this.http.get(`${this.apiUrl}/upload/slider`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } });
  }

  deleteSlider(id) {
    return this.http.delete(`${this.apiUrl}/upload/deleteSlider/${ id }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } });
  }

  getLogos() {
    return this.http.get(`${this.apiUrl}/upload/logos`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } });
  }

  deleteLogo(number) {
    return this.http.delete(`${this.apiUrl}/upload/deleteLogos/${ number }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } });
  }

  saveIVLink(data) {
    return this.http.put(`${this.apiUrl}/upload/saveIVLink`, data, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } });
  }
}
