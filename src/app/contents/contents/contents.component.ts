import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ContentsService} from '../contents.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../environments/environment';
import swal from "sweetalert2";

@Component({
  selector: 'app-contents',
  templateUrl: './contents.component.html',
  styleUrls: ['./contents.component.css']
})
export class ContentsComponent implements OnInit {
  public imageRootUrl = environment.mainIpUrl;
  public loading = false;
  public sliders;
  public logos;
  public selectedFiles = [];
  public formData = new FormData();
  form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private contentService: ContentsService,
  ) { }

  ngOnInit() {
    this.getSliders();
    this.getLogos();
    this.buildForm();
  }
  get comment() {
    return this.form.get('comment');
  }

  get buttonText() {
    return this.form.get('buttonText');
  }

  get link() {
    return this.form.get('link');
  }
  get index() {
    return this.form.get('index');
  }

  urls = [];
  urls1 = [];
  urls2 = [];
  logoUrls = [];

  onFileChanged1(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('image', this.selectedFiles[i]);
          this.urls1.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }
  onFileChanged2(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('image', this.selectedFiles[i]);
          this.urls2.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  deleteSliderImage(no) {
    swal({
      type: 'warning',
      title: 'Are you sure to Delete Slider Image?',
      text: 'You will not be able to recover the data of Slider Image',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.contentService
          .deleteSlider(no)
          .subscribe((result: any) => {
            this.urls = [];
            this.getSliders();
          }, (error: Response) => {});
      } else {
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Your Slider image is safe :)'
        });
      }
    });
  }

  deleteLogoImage(no) {
    swal({
      type: 'warning',
      title: 'Are you sure to Delete Logo Image?',
      text: 'You will not be able to recover the data of Logo Image',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
    this.contentService
      .deleteLogo(no)
      .subscribe((result: any) => {
        this.logoUrls = [];
        this.getLogos();
      }, (error: Response) => {});
      } else {
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Your Logo image is safe :)'
        });
      }
    });
  }

  getLogos() {
    this.contentService
      .getLogos()
      .subscribe((result: any) => {
        // for (let i = 0; i < result.contents.length; i += 1) {
        //   this.logoUrls.push(`${environment.mainIpUrl}/${result.contents[i].image}`);
        // }
        this.logos = result.contents;
      }, (error: Response) => {

      });
  }

  getSliders() {
    this.contentService
      .getSliders()
      .subscribe((result: any) => {
        for (let i = 0; i < result.contents.length; i += 1) {
          this.urls.push(`${environment.mainIpUrl}/${result.contents[i].image}`);
        }
        this.sliders = result.contents;
      }, (error: Response) => {

      });
  }

  uploadSlider() {
    this.loading = true;
    const data = {
      key: 'slider',
      comment: this.form.value.comment,
      buttonText: this.form.value.buttonText,
      number: this.form.value.index,
      link: this.form.value.link,
    };
    this.formData.append('data', JSON.stringify(data));
    this.contentService
      .uploadSliderImages(this.formData)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","Slider Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.urls1 = [];
            this.selectedFiles = [];
            this.form.reset();
            this.getSliders();
          })
          // this.router.navigate(['contents']);
          // setTimeout(() => {
          //   this.successMsg = 'News Created Successfully';
          // }, 2000);
        },
        (error: Response) => {
          this.loading = false;
          // setTimeout(() => {
          //   this.errorMsg = error;
          // }, 2000);
        }
      );
  }

  uploadLogo() {
    this.loading = true;
    const data = {
      link: this.form.value.link
    };
    this.formData.append('data', JSON.stringify(data));
    this.contentService
      .uploadLogoImage(this.formData)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","Logo Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.urls2 = [];
            this.selectedFiles = [];
            this.form.reset();
            this.getLogos();
          })
          // this.router.navigate(['contents']);
          // setTimeout(() => {
          //   this.successMsg = 'News Created Successfully';
          // }, 2000);
        },
        (error: Response) => {
          this.loading = false;
          // setTimeout(() => {
          //   this.errorMsg = error;
          // }, 2000);
        }
      );
  }

  buildForm() {
    this.form = this.fb.group({
      comment: [''],
      buttonText: [''],
      link: [''],
      index: ['']
    });
  }
}
