export const USER_ID = 'user-id';
export const AUTH_TOKEN = 'auth-token';
export const googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAVyrPpRVpBYwmDTYzZk0v0hp7B3KuhKpc?libraries=places';
export const ITEMS_PER_PAGE = 5;
export const NAME_REGEX = /^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/;
export const PHONE_REGEX = /^9665[0-9]{8}$$/;
export const EMAIL_REGEX = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
export const PASSWORD_REGEX = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
