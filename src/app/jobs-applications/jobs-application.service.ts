import { Injectable } from '@angular/core';
import {AUTH_TOKEN} from '../constants';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobsApplicationService {
  apiUrl = environment.apiBaseUrl;
  private userId: string = null;
  private _isAuthenticated = new BehaviorSubject(false);
  constructor(private http: HttpClient) { }

  getApplications(query) {
    return this.http.get( `${ this.apiUrl }/jobs/applications?${ query }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  getApplication(id, applicationId) {
    return this.http.get( `${ this.apiUrl }/jobs/${ id }/applications/${ applicationId }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  updateApplication(id, applicationId, data) {
    return this.http.put( `${ this.apiUrl }/jobs/${ id }/applications/${ applicationId }`, data, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  deleteApplication(id, applicationId) {
    return this.http.delete( `${ this.apiUrl }/jobs/${ id }/applications/${ applicationId }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }
}
