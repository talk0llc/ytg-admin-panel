import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {JobsApplicationService} from '../jobs-application.service';
import {DomSanitizer} from '@angular/platform-browser';
import swal from "sweetalert2";

@Component({
  selector: 'app-job-application-details',
  templateUrl: './job-application-details.component.html',
  styleUrls: ['./job-application-details.component.css']
})
export class JobApplicationDetailsComponent implements OnInit {
  public applicationId = this.route.snapshot.params.applicationId;
  public jobId = this.route.snapshot.params.id;
  public loading = false;
  public jobApplicationQA;
  public response = '';
  public grade = '';
  public status = '';
  public link = '';
  public email = '';
  public name = '';
  public mobile = '';
  public grade_interview = '';
  form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private jobApplicationService: JobsApplicationService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.getOne(this.jobId, this.applicationId);
    this.buildForm();
  }
  getOne(id, applicationId) {
    this.loading = true;
    this.jobApplicationService
      .getApplication(id, applicationId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.jobApplicationQA = result.questionAnswers;
          this.email = result.user.email;
          this.name = result.user.first_name;
          this.mobile = result.user.mobile;
          this.grade = result.grade;
          this.status = result.status;
          this.link = result.link;
          this.grade_interview = result.grade_interview;
          this.response = result.responseNote;
          this.buildForm();
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  update(id, applicationId, status) {
    const data = {
      grade: this.form.value.grade,
      grade_interview: this.form.value.grade_interview,
      responseNote: this.form.value.response,
      link: this.form.value.link,
      status
    };
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Update Application?',
      text: 'You are going to update the application status',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, update!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
    this.jobApplicationService
      .updateApplication(id, applicationId, data)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.router.navigate(['jobs/applications']);
        },
        (error: Response) => {
          this.loading = false;
        }
      );
      } else {
        this.loading = false;
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Keep Status'
        });
      }
    });
  }

  buildForm() {
    this.form = this.fb.group({
      response: [this.response],
      grade: [this.grade],
      link: [this.link],
      grade_interview: [this.grade_interview],
    });
  }
}
