import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {JobsService} from '../../jobs/jobs.service';
import swal from "sweetalert2";
declare var $: any;

@Component({
  selector: 'app-create-application',
  templateUrl: './create-application.component.html',
  styleUrls: ['./create-application.component.css']
})
export class CreateJobApplicationComponent implements OnInit {
  public ctrl = this;
  public jobId = this.route.snapshot.params.id;
  public types = [
    'Checkbox',
    'RadioButton',
    'Paragraph',
    'Short Answer',
    'Dropdown Menu',
    'File Upload'
  ];
  public answers = [];
  public answerCount = 0;
  public questions = [];
  public selectedFiles = [];
  public formData = new FormData();
  public loading = false;
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private jobService: JobsService,
  ) { }

  ngOnInit() {
    this.get(this.jobId);
    this.buildForm();
  }
  get question() {
    return this.form.get('question');
  }

  get answer() {
    return this.form.get('answer');
  }

  get type() {
    return this.form.get('type');
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  create() {
    for (let i = 1; i <= this.answerCount; i += 1) {
      const prevElement: any = document.getElementById('answer' + i);
      if (prevElement) {
        this.answers.push(prevElement.value);
      }
    }
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      question: this.form.value.question,
      answers: this.answers,
      type: this.form.value.type
    };
    this.loading = true;
    this.jobService
      .addQuestion(this.jobId, data)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","Application Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            this.get(this.jobId);
            const parent = document.getElementById('questionAnswers');
            for (let answerIndex = this.answerCount; answerIndex > 0; answerIndex -= 1) {
              parent.removeChild(parent.childNodes[answerIndex]);
            }
            this.answerCount = 0;
            this.answers = [];
            this.resetForm();
          })
        },
        (error: Response) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          $('.content').find('*').removeAttr('disabled');
          $('.content').find('a').unbind('click');
          this.get(this.jobId);
          const parent = document.getElementById('questionAnswers');
          for (let answerIndex = this.answerCount; answerIndex > 0; answerIndex -= 1) {
            parent.removeChild(parent.childNodes[answerIndex]);
          }
          this.answerCount = 0;
          this.answers = [];
          this.resetForm();
        }
      );
  }

  get(jobId) {
    this.loading = true;
    this.jobService
      .getQuestions(this.jobId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.questions = result;
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  delete(jobId, questionId) {
    this.loading = true;
    this.jobService
      .deleteQuestion(jobId, questionId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.get(jobId);
        },
        (error: Response) => {
          this.loading = false;
          this.get(jobId);
        }
      );
  }

  AddNewAnswer() {
    const addAnswerLabel = document.getElementById('answerLbl');
    addAnswerLabel.removeAttribute('hidden');
    this.answerCount += 1;
    const newAnswer = document.createElement('input');
    newAnswer.setAttribute('type', 'text');
    newAnswer.setAttribute('class', 'form-control');
    newAnswer.setAttribute('placeholder', 'Enter Answer');
    newAnswer.setAttribute('id', 'answer' + (this.answerCount));
    const parent = document.getElementById('questionAnswers');
    parent.appendChild(newAnswer);
  }

  RemoveAnswer() {
    const parent = document.getElementById('questionAnswers');
    parent.removeChild(parent.childNodes[this.answerCount]);
    this.answerCount -= 1;
  }

  onSelectType(type) {
    const addAnswerButton = document.getElementById('addAnswerBtn');
    const removeAnswerBtn = document.getElementById('removeAnswerBtn');
    const addAnswerLabel = document.getElementById('answerLbl');
    if (type.value === 'Short Answer' || type.value === 'Paragraph' || type.value === 'FileUpload') {
      addAnswerButton.setAttribute('disabled', 'disabled');
      removeAnswerBtn.setAttribute('disabled', 'disabled');
      if (addAnswerLabel) {
        addAnswerLabel.setAttribute('hidden', 'hidden');
        const parent = document.getElementById('questionAnswers');
        for (let answerIndex = this.answerCount; answerIndex > 0; answerIndex -= 1) {
          parent.removeChild(parent.childNodes[answerIndex]);
        }
      }
    } else if (type.value === 'Checkbox' || type.value === 'RadioButton' || type.value === 'Dropdown Menu') {
      addAnswerButton.removeAttribute('disabled');
      removeAnswerBtn.removeAttribute('disabled');
      if (addAnswerLabel) {
        addAnswerLabel.removeAttribute('hidden');
      }
    }
    this.answerCount = 0;
  }

  resetForm() {
    this.answerCount = 0;
    const addAnswerButton = document.getElementById('addAnswerBtn');
    const removeAnswerButton = document.getElementById('removeAnswerBtn');
    const addAnswerLabel = document.getElementById('answerLbl');
    addAnswerButton.setAttribute('disabled', 'disabled');
    removeAnswerButton.setAttribute('disabled', 'disabled');
    if (addAnswerLabel) {
      addAnswerLabel.setAttribute('hidden', 'hidden');
      const parent = document.getElementById('questionAnswers');
      for (let answerIndex = this.answerCount; answerIndex > 0; answerIndex -= 1) {
        parent.removeChild(parent.childNodes[answerIndex]);
      }
    }
    const questionInput: any = document.getElementById('questionInput');
    questionInput.value = '';
  }

  buildForm() {
    this.form = this.fb.group({
      question: ['', Validators.required],
      answer: ['', Validators.required],
      type: [this.types]
    });
  }
}
