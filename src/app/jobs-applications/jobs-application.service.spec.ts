import { TestBed } from '@angular/core/testing';

import { JobsApplicationService } from './jobs-application.service';

describe('JobsApplicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JobsApplicationService = TestBed.get(JobsApplicationService);
    expect(service).toBeTruthy();
  });
});
