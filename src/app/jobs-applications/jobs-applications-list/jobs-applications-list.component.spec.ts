import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsApplicationsListComponent } from './jobs-applications-list.component';

describe('JobsApplicationsListComponent', () => {
  let component: JobsApplicationsListComponent;
  let fixture: ComponentFixture<JobsApplicationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobsApplicationsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsApplicationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
