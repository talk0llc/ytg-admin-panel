import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {JobsApplicationService} from '../jobs-application.service';
import {JobsService} from '../../jobs/jobs.service';
import swal from "sweetalert2";
@Component({
  selector: 'app-jobs-applications-list',
  templateUrl: './jobs-applications-list.component.html',
  styleUrls: ['./jobs-applications-list.component.css']
})
export class JobsApplicationsListComponent implements OnInit {
  public jobId = this.route.snapshot.params.id;
  public jobs = [];
  public statusList = ['in-review', 'accepted', 'app-rejected', 'invited'];
  public query = '';
  public job = '';
  public status = '';
  public queryFilter = {
    email: '',
    status: '',
    title: ''
  };
  public loading = false;
  public nameFilter = '';
  public jobApplications = [];
  pageNo: number = 1;
  limit: number = 5;
  totalPages: number;
  public pagingQuery = `page=${ this.pageNo - 1 }&limit=${ this.limit}`;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private jobApplicationService: JobsApplicationService,
    private jobService: JobsService
  ) { }

  ngOnInit() {
    this.getJobApplications(this.pagingQuery, this.queryFilter);
    this.getJobs();
  }
  getJobs() {
    this.jobService
      .getAll('')
      .subscribe(
        (result: any) => {
          this.jobs = result.jobs;
          this.jobs.unshift({ title: 'All' });
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  filterByEmail (value) {
    if (value !== '') {
      this.queryFilter.email = `&email=${value}`;
    } else {
      this.queryFilter.email = '';
    }
    this.getJobApplications(this.pagingQuery, this.queryFilter);
  }

  filterByStatus (value) {
    if (value !== '') {
      this.queryFilter.status = `&status=${value}`;
    }
    this.getJobApplications(this.pagingQuery, this.queryFilter);
  }

  filterByJob (value) {
    if (value === 'All') {
      this.queryFilter.title = '';
    } else if (value !== '' && value !== 'All') {
      this.queryFilter.title = `&title=${value}`;
    }
    this.getJobApplications(this.pagingQuery, this.queryFilter);
  }

  getJobApplications(pagingQuery, queryFilter) {
    this.query = `${pagingQuery}${queryFilter.email}${queryFilter.status}${queryFilter.title}`;
    this.loading = true;
    this.jobApplicationService
      .getApplications(this.query)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.jobApplications = result.jobqas;
          this.totalPages = result.totalCount;
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  delete (jobId, id) {
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Delete Application?',
      text: 'You will not be able to recover the data of Application',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.jobApplicationService
          .deleteApplication(jobId, id)
          .subscribe(
            (result: any) => {
              this.loading = false;
              this.getJobApplications(this.pagingQuery, this.queryFilter);
              // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
              // this.router.navigate(['news']);
              // setTimeout(() => {
              //   this.successMsg = 'News Created Successfully';
              // }, 2000);
            },
            (error: Response) => {
              this.loading = false;
              this.getJobApplications(this.pagingQuery, this.queryFilter);
              // setTimeout(() => {
              //   this.errorMsg = error;
              // }, 2000);
            }
          );
      } else {
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Your Application is safe :)'
        });
      }
    });
  }


  pageChanged(nextPageNumber) {
    this.pagingQuery = `page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    this.pageNo = nextPageNumber;
    this.getJobApplications(this.pagingQuery, this.queryFilter);
  }

}
