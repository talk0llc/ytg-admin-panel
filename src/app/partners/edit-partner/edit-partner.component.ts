import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PartnersService } from '../partners.service';
declare var $;

@Component({
  selector: 'app-edit-partner',
  templateUrl: './edit-partner.component.html',
  styleUrls: ['./edit-partner.component.css']
})
export class EditPartnerComponent implements OnInit {
  public partnerId = this.route.snapshot.params.id;
  public ctrl = this;
  public is_active = false;
  public loading = false;
  public selectedFiles = [];
  public email =  '';
  public name =  '';
  public mobile =  '';
  public profession =  '';
  public entity =  '';
  public interestedIn =  '';
  public message =  '';
  public formData = new FormData();
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private partnerService: PartnersService,
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getOne(this.partnerId);
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('image', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  update() {
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      email: this.form.value.email,
      name: this.form.value.name,
      mobile: this.form.value.mobile,
      profession: this.form.value.profession,
      entity: this.form.value.entity,
      interestedIn: this.form.value.interestedIn,
      message: this.form.value.message,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.loading = true;
    this.partnerService
      .update(this.formData, this.partnerId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.router.navigate(['partners']);
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  getOne(id) {
    this.loading = true;
    this.partnerService
      .getOne(id)
      .subscribe(
        (result: any) => {
          this.email =  result.email;
          this.name =  result.name;
          this.mobile =  result.mobile;
          this.profession =  result.profession;
          this.entity =  result.entity;
          this.interestedIn =  result.interestedIn;
          // this.image =  this.urls;
          this.message =  result.message;
          this.is_active =  result.is_active;
          this.loading = false;

          this.buildForm();
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  buildForm() {
    this.form = this.fb.group({
      email: [this.email, Validators.required],
      name: [this.name, Validators.required],
      mobile: [this.mobile, Validators.required],
      profession: [this.profession],
      entity: [this.entity],
      interestedIn: [this.interestedIn],
      message: [this.message],
    });
  }

}

