import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PartnersService } from '../partners.service';
import swal from "sweetalert2";
declare var $;

@Component({
  selector: 'app-create-partner',
  templateUrl: './create-partner.component.html',
  styleUrls: ['./create-partner.component.css']
})
export class CreatePartnerComponent implements OnInit {
  public ctrl = this;
  public is_active = false;
  public loading = false;
  public selectedFiles = [];
  public formData = new FormData();
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private partnerService: PartnersService,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  get email() {
    return this.form.get('email');
  }
  get mobile() {
    return this.form.get('mobile');
  }
  get profession() {
    return this.form.get('profession');
  }
  get entity() {
    return this.form.get('entity');
  }
  get interestedIn() {
    return this.form.get('interestedIn');
  }
  get message() {
    return this.form.get('message');
  }
  get name() {
    return this.form.get('name');
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('image', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  create() {
    this.loading = true;
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      email: this.form.value.email,
      name: this.form.value.name,
      mobile: this.form.value.mobile,
      profession: this.form.value.profession,
      entity: this.form.value.entity,
      interestedIn: this.form.value.interestedIn,
      message: this.form.value.message,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.partnerService
      .create(this.formData)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","Partner Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.router.navigate(['partners']);
          })
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }
  buildForm() {
    this.form = this.fb.group({
      email: ['', Validators.required],
      name: ['', Validators.required],
      mobile: ['', Validators.required],
      profession: [''],
      entity: [''],
      interestedIn: [''],
      message: [''],
    });
  }
}
