import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AUTH_TOKEN, USER_ID} from '../constants';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  apiUrl = environment.apiBaseUrl;
  private userId: string = null;
  private _isAuthenticated = new BehaviorSubject(false);
  constructor(private http: HttpClient) { }

  create(data) {
    return this.http.post(`${this.apiUrl}/users`, data, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } });
  }

  update(data, id) {
    return this.http.put( `${ this.apiUrl }/users/${ id }`, data, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  getOne(id) {
    return this.http.get( `${ this.apiUrl }/users/${ id }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  getAll(query) {
    return this.http.get( `${ this.apiUrl }/users?${ query }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  delete(id) {
    return this.http.delete( `${ this.apiUrl }/users/${ id }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }
}
