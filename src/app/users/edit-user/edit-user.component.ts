import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../users.service';
declare var $;

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  public ctrl = this;
  public userId = this.route.snapshot.params.id;
  public email =  '';
  public password =  '';
  public first_name =  '';
  public last_name =  '';
  public mobile =  '';
  public gender =  '';
  public image =  '';
  public date_of_birth = '';
  public residence_address = '';
  public graduation_status = '';
  public organization = '';
  public is_active = false;
  public loading = false;
  public selectedFiles = [];
  public genders = ['Male', 'Female'];
  form: FormGroup;
  message: string;
  public formData = new FormData();
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UsersService,
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getOne(this.userId);
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  update() {
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      email: this.form.value.email,
      first_name: this.form.value.first_name,
      last_name: this.form.value.last_name,
      mobile: this.form.value.mobile,
      gender: this.form.value.gender,
      date_of_birth: this.form.value.date_of_birth,
      residence_address: this.form.value.residence_address,
      graduation_status: this.form.value.graduation_status,
      organization: this.form.value.organization,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.loading = true;
    this.userService
      .update(this.formData, this.userId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.router.navigate(['users']);
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  getOne(id) {
    this.loading = true;
    this.urls = [];
    this.userService
      .getOne(id)
      .subscribe(
        (result: any) => {
          this.email =  result.email;
          this.password =  result.password;
          this.first_name =  result.first_name;
          this.last_name =  result.last_name;
          this.mobile =  result.mobile;
          this.gender =  result.gender;
          this.date_of_birth =  result.date_of_birth;
          this.residence_address =  result.residence_address;
          this.graduation_status =  result.graduation_status;
          this.organization =  result.organization;
          this.urls.push(result.image);
          this. is_active =  result.is_active;
          this.loading = false;
          this.buildForm();
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }
  buildForm() {
    this.form = this.fb.group({
      email: [this.email, Validators.required],
      password: [this.password, Validators.required],
      first_name: [this.first_name, Validators.required],
      last_name: [this.last_name, Validators.required],
      mobile: [this.mobile, Validators.required],
      gender: [this.gender],
      date_of_birth: [this.date_of_birth],
      residence_address: [this.residence_address],
      graduation_status: [this.graduation_status],
      organization: [this.organization],
    });
  }

}
