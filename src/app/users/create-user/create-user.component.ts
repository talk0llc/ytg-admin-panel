import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {UsersService } from '../users.service';
import swal from "sweetalert2";
declare var $;

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  public ctrl = this;
  public genders = ['Male', 'Female'];
  public is_active = false;
  public loading = false;
  public selectedFiles = [];
  public formData = new FormData();
  form: FormGroup;
  message: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UsersService,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  get email() {
    return this.form.get('email');
  }
  get password() {
    return this.form.get('password');
  }
  get first_name() {
    return this.form.get('first_name');
  }
  get last_name() {
    return this.form.get('last_name');
  }
  get mobile() {
    return this.form.get('mobile');
  }
  get gender() {
    return this.form.get('gender');
  }
  get date_of_birth() {
    return this.form.get('date_of_birth');
  }
  get graduation_status() {
    return this.form.get('graduation_status');
  }
  get residence_address() {
    return this.form.get('residence_address');
  }
  get organization() {
    return this.form.get('organization');
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  create() {
    this.loading = true;
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      email: this.form.value.email,
      password: this.form.value.password,
      first_name: this.form.value.first_name,
      last_name: this.form.value.last_name,
      mobile: this.form.value.mobile,
      gender: this.form.value.gender,
      date_of_birth: this.form.value.date_of_birth,
      residence_address: this.form.value.residence_address,
      graduation_status: this.form.value.graduation_status,
      organization: this.form.value.organization,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.userService
      .create(this.formData)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","User Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.router.navigate(['users']);
          })
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  buildForm() {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      mobile: ['', Validators.required],
      gender: [this.genders],
      date_of_birth: [''],
      residence_address: [''],
      organization: [''],
      graduation_status: [''],
    });
  }

}
