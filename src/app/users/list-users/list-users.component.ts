import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UsersService} from '../../users/users.service';
import swal from 'sweetalert2';

declare var $;
@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  public ctrl = this;
  public loading = false;
  public users = [];
  public nameFilter = '';
  successMsg: string;
  errorMsg: string;
  public query = '';
  pageNo: number = 1;
  limit: number = 5;
  totalPages: number;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UsersService,
  ) { }

  ngOnInit() {
    this.query = `page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    this.getAll(this.query);
  }

  filter (value) {
    if (value !== '') {
      this.query = `email=${value}&first_name=${value}&page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    } else {
      this.query = `page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    }
    this.getAll(this.query);
  }

  getAll(query) {
    this.loading = true;
    this.userService
      .getAll(query)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.users = result.users;
          this.totalPages = result.totalCount;
          // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          // this.router.navigate(['news']);
          // setTimeout(() => {
          //   this.successMsg = 'News Created Successfully';
          // }, 2000);
        },
        (error: Response) => {
          this.loading = false;
          // setTimeout(() => {
          //   this.errorMsg = error;
          // }, 2000);
        }
      );
  }

  delete (id) {
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Delete User?',
      text: 'You will not be able to recover the data of User',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.userService
          .delete(id)
          .subscribe(
            (result: any) => {
              this.loading = false;
              this.getAll(this.query);
              // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
              // this.router.navigate(['news']);
              // setTimeout(() => {
              //   this.successMsg = 'News Created Successfully';
              // }, 2000);
            },
            (error: Response) => {
              this.loading = false;
              this.getAll(this.query);
              // setTimeout(() => {
              //   this.errorMsg = error;
              // }, 2000);
            }
          );
      } else {
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Your User file is safe :)'
        });
      }
    });
  }

  pageChanged(nextPageNumber) {
    if (this.nameFilter !== '') {
      this.query = `email=${this.nameFilter}&first_name=${this.nameFilter}&page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    } else {
      this.query = `page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    }
    this.pageNo = nextPageNumber;
    this.getAll(this.query);
  }
}
