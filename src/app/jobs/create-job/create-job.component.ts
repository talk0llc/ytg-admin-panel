import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { JobsService } from '../jobs.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import swal from "sweetalert2";

declare var $: any;
@Component({
  selector: 'app-create-job',
  templateUrl: './create-job.component.html',
  styleUrls: ['./create-job.component.css']
})
export class CreateJobComponent implements OnInit {
  public ctrl = this;
  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
  };
  public pinned = true;
  public is_active = false;
  public loading = false;
  public categories = ['Full-Time', 'Part-Time', 'Internship'];
  public segments = [
    { color: 'Red', code: '#ff5a61', text: 'Volunteer' },
    { color: 'Blue', code: '#04adc7', text: 'Team' },
  ];
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private jobService: JobsService,
  ) { }

  ngOnInit() {
    $(function () {
      // Date picker
      $('#datepicker').datepicker({
        autoclose: true
      });
    });
    this.buildForm();
  }

  get title() {
    return this.form.get('title');
  }

  get description() {
    return this.form.get('description');
  }

  get category() {
    return this.form.get('category');
  }

  get deadline() {
    return this.form.get('deadline');
  }

  get segment() {
    return this.form.get('segment');
  }

  create() {
    this.loading = true;
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    this.jobService
      .create({
        title: this.form.value.title,
        description: this.form.value.description,
        segment: this.form.value.segment,
        category: this.form.value.category,
        deadline: this.form.value.deadline,
        pinned: this.pinned,
        is_active: this.is_active
      })
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","Job Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.router.navigate(['jobs']);
          })
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }


  buildForm() {
    this.form = this.fb.group({
      title: ['', Validators.required],
      description: [''],
      segment: [this.segments],
      deadline: [''],
      category: [this.categories],
    });
  }

}
