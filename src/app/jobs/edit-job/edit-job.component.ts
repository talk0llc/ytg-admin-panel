import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { JobsService } from '../jobs.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {environment} from '../../../environments/environment';
declare var $: any;
@Component({
  selector: 'app-edit-job',
  templateUrl: './edit-job.component.html',
  styleUrls: ['./edit-job.component.css']
})
export class EditJobComponent implements OnInit {
  public jobId = this.route.snapshot.params.id;
  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
  };
  public mainIpUrl = environment.mainIpUrl;
  public editorConfig2: AngularEditorConfig = {
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: `/app/images/uploads`, // if needed
  };
  public pinned = true;
  public title = '';
  public description = '';
  public category = '';
  public categories = ['Full-Time', 'Part-Time', 'Internship'];
  public segment = '';
  public segments = [
    { color: 'Red', code: '#ff5a61', text: 'Volunteer' },
    { color: 'Blue', code: '#04adc7', text: 'Team' },
  ];
  public sections = ['Text', 'File'];
  public deadline: string;
  public salary = '';
  public is_active = false;
  public loading = false;
  public selectedFiles = [];
  public extraSections = [];
  public baseUrl = environment.mainIpUrl;
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  public formData = new FormData();
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private jobService: JobsService,
  ) { }

  ngOnInit() {
    $(function () {
      // Date picker
      $('#datepicker').datepicker({
        autoclose: true
      });
    });
    this.buildForm();
    this.getOne(this.jobId);
  }
  get section() {
    return this.form.get('section');
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  update() {
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      title: this.form.value.title,
      description: this.form.value.description,
      segment: this.form.value.segment,
      category: this.form.value.category,
      deadline: this.deadline,
      pinned: this.pinned,
      is_active: this.is_active
    };
    this.loading = true;
    this.jobService
      .update(data, this.jobId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.formData = new FormData();
          this.router.navigate(['jobs']);
        },
        (error: Response) => {
          this.loading = false;
          this.formData = new FormData();
        }
      );
  }

  getOne(id) {
    this.loading = true;
    this.jobService
      .getOne(id)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.title = result.title;
          this.description = result.description;
          this.salary = result.salary;
          this.segment = result.segment;
          this.category = result.category;
          this.deadline = new Date(result.deadline).toLocaleDateString();
          // this.urls = result.images;
          this.pinned = result.pinned;
          this.is_active = result.is_active;
          this.extraSections = result.extraSections;
          this.buildForm();
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  updateSections() {
    const data = {
      extraSections: {
        // images: this.urls,
        description: this.form.value.extraDescription
      }
    };
    this.formData.append('data', JSON.stringify(data));
    this.loading = true;
    this.jobService
      .update(this.formData, this.jobId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.urls = [];
          this.form.controls['extraDescription'].setValue('');
          this.formData = new FormData();
          this.getOne(this.jobId);
          this.reset();
        },
        (error: Response) => {
          this.loading = false;
          this.formData = new FormData();
          this.getOne(this.jobId);
          this.reset();
        }
      );
  }

  onSelectType(type) {
    const description = document.getElementById('description');
    const file = document.getElementById('fileupload');
    const saveButton = document.getElementById('saveButton');
    if (type.value === 'Text') {
      description.removeAttribute('hidden');
      file.setAttribute('hidden', 'hidden');
    } else {
      file.removeAttribute('hidden');
      description.setAttribute('hidden', 'hidden');
    }
    saveButton.removeAttribute('disabled');
  }

  reset() {
    const description = document.getElementById('description');
    const file = document.getElementById('fileupload');
    const saveButton = document.getElementById('saveButton');
    file.setAttribute('hidden', 'hidden');
    description.setAttribute('hidden', 'hidden');
    saveButton.setAttribute('disabled', 'disabled');
  }

  deleteSection(sectionId) {
    this.loading = true;
    this.jobService
      .deleteSection(this.jobId, sectionId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.getOne(this.jobId);
        },
        (error: Response) => {
          this.loading = false;
          this.getOne(this.jobId);
        }
      );
  }

  buildForm() {
    this.form = this.fb.group({
      title: [this.title, Validators.required],
      description: [this.description, Validators.required],
      is_active: [this.is_active],
      pinned: [this.pinned],
      category: [this.category],
      segment: [this.segment],
      section: [this.sections],
      deadline: [this.deadline],
      extraDescription: ['']
    });
  }

}
