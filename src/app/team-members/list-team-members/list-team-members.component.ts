import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { TeamMembersService } from '../team-members.service';
import swal from 'sweetalert2';

declare var $;
@Component({
  selector: 'app-list-team-members',
  templateUrl: './list-team-members.component.html',
  styleUrls: ['./list-team-members.component.css']
})
export class ListTeamMembersComponent implements OnInit {
  public ctrl = this;
  public loading = false;
  public members = [];
  public nameFilter = '';
  successMsg: string;
  errorMsg: string;
  public query = '';
  pageNo: number = 1;
  limit: number = 5;
  totalPages: number;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private teamMembersService: TeamMembersService,
  ) { }

  ngOnInit() {
    this.query = `page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    this.getAll(this.query);
  }
  filter (value) {
    if (value !== '') {
      this.query = `name=${value}&page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    } else {
      this.query = `page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    }
    this.getAll(this.query);
  }
  getAll(query) {
    this.loading = true;
    this.teamMembersService
      .getAll(query)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.members = result.teammembers;
          this.totalPages = result.totalCount;
          // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          // this.router.navigate(['news']);
          // setTimeout(() => {
          //   this.successMsg = 'News Created Successfully';
          // }, 2000);
        },
        (error: Response) => {
          this.loading = false;
          // setTimeout(() => {
          //   this.errorMsg = error;
          // }, 2000);
        }
      );
  }

  delete (id) {
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Delete Team Member?',
      text: 'You will not be able to recover the data of Team Member',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.teamMembersService
          .delete(id)
          .subscribe(
            (result: any) => {
              this.loading = false;
              this.getAll(this.query);
            },
            (error: Response) => {
              this.loading = false;
              this.getAll(this.query);
            }
          );
      } else {
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Your Team Member file is safe :)'
        });
      }
    });
  }

  pageChanged(nextPageNumber) {
    if (this.nameFilter !== '') {
      this.query = `name=${this.nameFilter}&page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    } else {
      this.query = `page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    }
    this.pageNo = nextPageNumber;
    this.getAll(this.query);
  }

}
