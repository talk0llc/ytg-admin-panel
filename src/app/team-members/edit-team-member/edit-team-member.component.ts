import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TeamMembersService } from '../team-members.service';
import {environment} from '../../../environments/environment';
declare var $;

@Component({
  selector: 'app-edit-team-member',
  templateUrl: './edit-team-member.component.html',
  styleUrls: ['./edit-team-member.component.css']
})
export class EditTeamMemberComponent implements OnInit {
  public memberId = this.route.snapshot.params.id;
  public name =  '';
  public title =  '';
  public linkedinUrl =  '';
  public images =  [];
  public pinned = false;
  public loading = false;
  public selectedFiles = [];
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  public formData = new FormData();
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private teamMemberService: TeamMembersService,
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getOne(this.memberId);
  }

  urls1 = [];
  urls2 = [];
  onFileChanged(event, no) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();
        reader.onload = (event: any) => {
          if (no === 1) {
            this.urls1.push(event.target.result);
            this.formData.append('images', this.selectedFiles[i]);
          } else {
            this.urls2.push(event.target.result);
            this.formData.append('images', this.selectedFiles[i]);
          }
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  update() {
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      name: this.form.value.name,
      title: this.form.value.title,
      linkedinUrl: this.form.value.linkedinUrl,
      pinned: this.pinned,
    };
    this.formData.append('data', JSON.stringify(data));
    this.loading = true;
    this.teamMemberService
      .update(this.formData, this.memberId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.router.navigate(['team-member']);
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  getOne(id) {
    this.loading = true;
    this.teamMemberService
      .getOne(id)
      .subscribe(
        (result: any) => {
          this.name =  result.name;
          this.title =  result.title;
          this.linkedinUrl =  result.linkedinUrl;
          this.pinned =  result.pinned;
          this.loading = false;
          this.urls1.push(`${ environment.mainIpUrl }/${ result.image1 }`);
          this.urls2.push(`${ environment.mainIpUrl }/${ result.image2 }`);
          this.buildForm();
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  buildForm() {
    this.form = this.fb.group({
      name: [this.name, Validators.required],
      title: [this.title, Validators.required],
      linkedinUrl: [this.linkedinUrl],
    });
  }


}
