import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TeamMembersService } from '../team-members.service';
import swal from "sweetalert2";
declare var $;

@Component({
  selector: 'app-create-team-member',
  templateUrl: './create-team-member.component.html',
  styleUrls: ['./create-team-member.component.css']
})
export class CreateTeamMemberComponent implements OnInit {
  public ctrl = this;
  public pinned = false;
  public loading = false;
  public selectedFiles = [];
  public formData = new FormData();
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private teamMemberService: TeamMembersService,
  ) { }

  ngOnInit() {
    this.buildForm();
  }
  get name() {
    return this.form.get('name');
  }
  get title() {
    return this.form.get('title');
  }
  get linkedinUrl() {
    return this.form.get('linkedinUrl');
  }

  urls1 = [];
  urls2 = [];
  onFileChanged(event, no) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();
        reader.onload = (event: any) => {
          if (no === 1) {
            this.urls1.push(event.target.result);
            this.formData.append('images', this.selectedFiles[i]);
          } else {
            this.urls2.push(event.target.result);
            this.formData.append('images', this.selectedFiles[i]);
          }
          };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  create() {
    this.loading = true;
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      name: this.form.value.name,
      title: this.form.value.title,
      linkedinUrl: this.form.value.linkedinUrl,
      pinned: this.pinned,
    };
    this.formData.append('data', JSON.stringify(data));
    this.teamMemberService
      .create(this.formData)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","Team Member Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.router.navigate(['team-member']);
          })
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }
  buildForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      title: ['', Validators.required],
      linkedinUrl: [''],
    });
  }
}
