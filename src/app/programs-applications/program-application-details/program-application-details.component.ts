import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ProgramsApplicationsService} from '../programs-applications.service';
import swal from 'sweetalert2';

import {DomSanitizer} from '@angular/platform-browser';
@Component({
  selector: 'app-program-application-details',
  templateUrl: './program-application-details.component.html',
  styleUrls: ['./program-application-details.component.css']
})
export class ProgramApplicationDetailsComponent implements OnInit {
  public applicationId = this.route.snapshot.params.applicationId;
  public programId = this.route.snapshot.params.id;
  public loading = false;
  public programApplicationQA;
  public response = '';
  public grade = '';
  public status = '';
  public link = '';
  public email = '';
  public name = '';
  public mobile = '';
  public grade_interview = '';
  file_flag:boolean = false;
  form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private programApplicationService: ProgramsApplicationsService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.getOne(this.programId, this.applicationId);
    this.buildForm();
  }
  getOne(id, applicationId) {
    this.loading = true;
    this.programApplicationService
      .getApplication(id, applicationId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.programApplicationQA = result.questionAnswers;
          this.email = result.user.email;
          this.name = result.user.first_name;
          this.mobile = result.user.mobile;
          this.grade = result.grade;
          this.link = result.link;
          this.status = result.status;
          this.grade_interview = result.grade_interview;
          this.response = result.responseNote;
          this.buildForm();
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  decode_inside(source){
    return decodeURIComponent(source);
  }

  getSecure(src){
    let feed = this.sanitizer.bypassSecurityTrustHtml(src);
    // this.file_flag=true;
    return feed;
    // return this.sanitizer.bypassSecurityTrustUrl(src);
  }

  update(id, applicationId, status) {
    const data = {
      grade: this.form.value.grade,
      link: this.form.value.link,
      grade_interview: this.form.value.grade_interview,
      responseNote: this.form.value.response,
      status
    };
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Update Application?',
      text: 'You are going to update the application status',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, update!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.programApplicationService
          .updateApplication(id, applicationId, data)
          .subscribe(
            (result: any) => {
              this.loading = false;
              const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
              this.router.navigate(['programs/applications']);
            },
            (error: Response) => {
              this.loading = false;
            }
          );
      } else {
        this.loading = false;
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Keep Status'
        });
      }
    });
  }

  buildForm() {
    this.form = this.fb.group({
      response: [this.response],
      grade: [this.grade],
      link: [this.link],
      grade_interview: [this.grade_interview],
    });
  }

  downloadPdf() {
    let data: any = [];

     for (var i = 0; i < this.programApplicationQA.length ; i++) {
       let counter = i + 1;
      let object = {
        none: 'Question '+counter,
        QuestionBody: this.decode_inside(this.programApplicationQA[i].question),
        AnswerBody: this.decode_inside(this.programApplicationQA[i].answer)
        }
        data.push(object);
      }

    this.programApplicationService.exportAsExcelFile(data, this.name);

  }

}
