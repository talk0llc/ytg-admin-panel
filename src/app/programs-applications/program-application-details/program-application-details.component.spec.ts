import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramApplicationDetailsComponent } from './program-application-details.component';

describe('ProgramApplicationDetailsComponent', () => {
  let component: ProgramApplicationDetailsComponent;
  let fixture: ComponentFixture<ProgramApplicationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramApplicationDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramApplicationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
