import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AUTH_TOKEN, USER_ID} from '../constants';
import { BehaviorSubject, Observable } from 'rxjs';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class ProgramsApplicationsService {
  apiUrl = environment.apiBaseUrl;
  private userId: string = null;
  private _isAuthenticated = new BehaviorSubject(false);
  constructor(private http: HttpClient) { }

  getApplications(query) {
    return this.http.get( `${ this.apiUrl }/programs/applications?${ query }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  getApplication(id, applicationId) {
    return this.http.get( `${ this.apiUrl }/programs/${ id }/applications/${ applicationId }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  updateApplication(id, applicationId, data) {
    return this.http.put( `${ this.apiUrl }/programs/${ id }/applications/${ applicationId }`, data,{ headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }


  deleteApplication(id, applicationId) {
    return this.http.delete( `${ this.apiUrl }/programs/${ id }/applications/${ applicationId }`, { headers: { 'Authorization': localStorage.getItem(AUTH_TOKEN) } } );
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
     const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
     FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
  }
}
