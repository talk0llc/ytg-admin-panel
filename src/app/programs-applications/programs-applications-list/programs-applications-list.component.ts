import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ProgramsApplicationsService} from '../programs-applications.service';
import {ProgramsService} from '../../programs/programs.service';
import swal from "sweetalert2";
@Component({
  selector: 'app-programs-applications-list',
  templateUrl: './programs-applications-list.component.html',
  styleUrls: ['./programs-applications-list.component.css']
})
export class ProgramsApplicationsListComponent implements OnInit {
  public programId = this.route.snapshot.params.id;
  public programs = [];
  public statusList = ['in-review', 'accepted', 'app-rejected', 'invited'];
  public query = '';
  public program = '';
  public status = '';
  public queryFilter = {
    email: '',
    status: '',
    name: ''
  };
  public loading = false;
  public nameFilter = '';
  public programApplications = [];
  public programs_paging:any = localStorage.getItem('programs_paging');
  public pagingQuery;
  pageNo: number = 1;
  limit: number = 5;
  totalPages: number;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private programApplicationService: ProgramsApplicationsService,
    private programService: ProgramsService
  ) { }

  ngOnInit() {
    if(this.programs_paging){
      this.pagingQuery = `page=${ this.programs_paging - 1 }&limit=${ this.limit}`;
      this.pageChanged(this.programs_paging);
    }
    else{
      this.pagingQuery = `page=${ this.pageNo - 1 }&limit=${ this.limit}`;
    }
    this.getProgramApplications(this.pagingQuery, this.queryFilter);
    this.getPrograms();
  }
  getPrograms() {
    this.programService
      .getAll('')
      .subscribe(
        (result: any) => {
          this.programs = result.programs;
          this.programs.unshift({ name: 'All' });
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }
  filterByEmail (value) {
    if (value !== '') {
      this.queryFilter.email = `&email=${value}`;
    } else {
      this.queryFilter.email = '';
    }
    this.getProgramApplications(this.pagingQuery, this.queryFilter);
  }

  filterByStatus (value) {
    if (value !== '') {
      this.queryFilter.status = `&status=${value}`;
    }
    this.getProgramApplications(this.pagingQuery, this.queryFilter);
  }

  filterByProgram (value) {
    if (value === 'All') {
      this.queryFilter.name = '';
    } else if (value !== '' && value !== 'All') {
      value = value.replace('&', '%26');
      this.queryFilter.name = `&name=${value}`;
    }
    this.getProgramApplications(this.pagingQuery, this.queryFilter);
  }

  getProgramApplications(pagingQuery, queryFilter) {
    this.query = `${pagingQuery}${queryFilter.email}${queryFilter.status}${queryFilter.name}`;
    this.loading = true;
    this.programApplicationService
      .getApplications(this.query)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.programApplications = result.programqas;
          this.totalPages = result.totalCount;
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  delete (programId, id) {
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Delete Application?',
      text: 'You will not be able to recover the data of Application',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.programApplicationService
          .deleteApplication(programId, id)
          .subscribe(
            (result: any) => {
              this.loading = false;
              this.getProgramApplications(this.pagingQuery, this.queryFilter);
              // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
              // this.router.navigate(['news']);
              // setTimeout(() => {
              //   this.successMsg = 'News Created Successfully';
              // }, 2000);
            },
            (error: Response) => {
              this.loading = false;
              this.getProgramApplications(this.pagingQuery, this.queryFilter);
              // setTimeout(() => {
              //   this.errorMsg = error;
              // }, 2000);
            }
          );
      } else {
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Your Application is safe :)'
        });
      }
    });
  }

  pageChanged(nextPageNumber) {
    localStorage.setItem('programs_paging', nextPageNumber);
    this.pagingQuery = `page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    this.pageNo = nextPageNumber;
    this.getProgramApplications(this.pagingQuery, this.queryFilter);
  }
}
