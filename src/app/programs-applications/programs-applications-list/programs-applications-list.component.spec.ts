import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramsApplicationsListComponent } from './programs-applications-list.component';

describe('ProgramsApplicationsListComponent', () => {
  let component: ProgramsApplicationsListComponent;
  let fixture: ComponentFixture<ProgramsApplicationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramsApplicationsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramsApplicationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
