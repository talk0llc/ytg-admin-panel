import { TestBed } from '@angular/core/testing';

import { ProgramsApplicationsService } from './programs-applications.service';

describe('ProgramsApplicationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProgramsApplicationsService = TestBed.get(ProgramsApplicationsService);
    expect(service).toBeTruthy();
  });
});
