import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsersService} from '../users/users.service';
import {ProgramsApplicationsService} from '../programs-applications/programs-applications.service';
import {EventsApplicationsService} from '../events-applications/events-applications.service';
import {JobsApplicationService} from '../jobs-applications/jobs-application.service';
import {PartnersService} from '../partners/partners.service';
import {StaffService} from '../staff/staff.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

  users:any;
  programs:any;
  events:any;
  jobs:any;
  partners:any;
  staff:any;

  constructor(private userService: UsersService,
    private programsService: ProgramsApplicationsService,
    private eventsService: EventsApplicationsService,
    private jobsService: JobsApplicationService,
    private partnersService: PartnersService,
    private staffService: StaffService
    ) { 
   }

  ngOnInit() {
    document.body.className = 'sidebar-mini skin-green-light';
    this.numbers();
  }

  numbers() {
    let query = `page=0&limit=5`;

    this.userService.getAll(query).subscribe(
      (result: any) => {
        this.users = result.totalCount;
      },
      (error: Response) => {}
    );

    this.programsService.getApplications(query).subscribe(
      (result: any) => {
        this.programs = result.totalCount;
      },
      (error: Response) => {}
    );

    this.eventsService.getApplications(query).subscribe(
      (result: any) => {
        this.events = result.totalCount;
      },
      (error: Response) => {}
    );

    this.jobsService.getApplications(query).subscribe(
      (result: any) => {
        this.jobs = result.totalCount;
      },
      (error: Response) => {}
    );

    this.partnersService.getAll(query).subscribe(
      (result: any) => {
        this.partners = result.totalCount;
      },
      (error: Response) => {}
    );

    this.staffService.getAll(query).subscribe(
      (result: any) => {
        this.staff = result.totalCount;
      },
      (error: Response) => {}
    );

  }

  ngOnDestroy(): void {
    document.body.className = '';
  }

}
