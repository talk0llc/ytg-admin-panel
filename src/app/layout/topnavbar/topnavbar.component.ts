import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-topnavbar',
  templateUrl: './topnavbar.component.html',
  styleUrls: ['./topnavbar.component.css']
})
export class TopnavbarComponent implements OnInit {
  first_name:string;
  last_name:string;
  image:string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.first_name = localStorage.getItem('fname');
    this.last_name = localStorage.getItem('lname');
    const images = localStorage.getItem('images');
    this.image = images && images.length > 0 ? `https://ytg.eco/${images}` : 'assets/dist/img/user2-160x160.jpg';
  }

  signOut() {
    this.authService.logout();
    this.router.navigate(['']);
  }

}
