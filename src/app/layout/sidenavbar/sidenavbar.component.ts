import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { StaffService } from '../../staff/staff.service';
import {USER_ID} from '../../constants';
declare var $;
@Component({
  selector: 'app-sidenavbar',
  templateUrl: './sidenavbar.component.html',
  styleUrls: ['./sidenavbar.component.css']
})
export class SidenavbarComponent implements OnInit {
  public userId = localStorage.getItem(USER_ID);
  image:string;
  public roles = [
    { key: 'Programs', value: 'programs', checked: false},
    { key: 'News', value: 'news', checked: false},
    { key: 'Jobs', value: 'jobs', checked: false},
    { key: 'Events', value: 'events', checked: false},
    { key: 'Staff', value: 'employees', checked: false},
    { key: 'Users', value: 'users', checked: false},
    { key: 'Services', value: 'services', checked: false},
    { key: 'Partners', value: 'partners', checked: false},
    { key: 'Team Members', value: 'members', checked: false},
    { key: 'Contents', value: 'contents', checked: false},
    { key: 'Applications', value: 'applications', checked: false},
  ];
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private staffService: StaffService,
  ) { }

  ngOnInit() {
    const images = localStorage.getItem('images');
    this.image = images && images.length > 0 ? `https://ytg.eco/${images}` : 'assets/dist/img/user2-160x160.jpg';
    $('ul').tree()
    this.getOne();
  }

  getOne() {
    this.staffService
      .getOne(this.userId)
      .subscribe(
        (result: any) => {
          for (let i = 0; i < result.roles[0].resources.length; i += 1) {
            for (let j = 0; j < this.roles.length; j += 1) {
              if (result.roles[0].resources[i].resource === this.roles[j].value) {
                this.roles[j].checked = true;
              }
            }
          }
        },
        (error: Response) => {
        }
      );
  }

}
