import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EventsApplicationsService} from '../events-applications.service';
import {EventsService} from '../../events/events.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-events-applications-list',
  templateUrl: './events-applications-list.component.html',
  styleUrls: ['./events-applications-list.component.css']
})
export class EventsApplicationsListComponent implements OnInit {
  public eventId = this.route.snapshot.params.id;
  public events = [];
  public statusList = ['in-review', 'accepted', 'app-rejected'];
  public query = '';
  public event = '';
  public status = '';
  public queryFilter = {
    email: '',
    status: '',
    title: ''
  };
  public loading = false;
  public nameFilter = '';
  public eventApplications = [];
  pageNo: number = 1;
  limit: number = 5;
  totalPages: number;
  public pagingQuery = `page=${ this.pageNo - 1 }&limit=${ this.limit}`;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private eventApplicationService: EventsApplicationsService,
    private eventService : EventsService
  ) { }

  ngOnInit() {
    this.getEventApplications(this.pagingQuery, this.queryFilter);
    this.getEvents();
  }
  getEvents() {
    this.eventService
      .getAll('')
      .subscribe(
        (result: any) => {
          this.events = result.events;
          this.events.unshift({ title: 'All' });
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  filterByEmail (value) {
    if (value !== '') {
      this.queryFilter.email = `&email=${value}`;
    } else {
      this.queryFilter.email = '';
    }
    this.getEventApplications(this.pagingQuery, this.queryFilter);
  }

  filterByStatus (value) {
    if (value !== '') {
      this.queryFilter.status = `&status=${value}`;
    }
    this.getEventApplications(this.pagingQuery, this.queryFilter);
  }

  filterByEvent (value) {
    if (value === 'All') {
      this.queryFilter.title = '';
    } else if (value !== '' && value !== 'All') {
      this.queryFilter.title = `&title=${value}`;
    }
    this.getEventApplications(this.pagingQuery, this.queryFilter);
  }

  getEventApplications(pagingQuery, queryFilter) {
    this.query = `${pagingQuery}${queryFilter.email}${queryFilter.status}${queryFilter.title}`;
    this.loading = true;
    this.eventApplicationService
      .getApplications(this.query)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.eventApplications = result.eventqas;
          this.totalPages = result.totalCount;
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  delete (eventId, id) {
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Delete Application?',
      text: 'You will not be able to recover the data of Application',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.eventApplicationService
          .deleteApplication(eventId, id)
          .subscribe(
            (result: any) => {
              this.loading = false;
              this.getEventApplications(this.pagingQuery, this.queryFilter);
              // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
              // this.router.navigate(['news']);
              // setTimeout(() => {
              //   this.successMsg = 'News Created Successfully';
              // }, 2000);
            },
            (error: Response) => {
              this.loading = false;
              this.getEventApplications(this.pagingQuery, this.queryFilter);
              // setTimeout(() => {
              //   this.errorMsg = error;
              // }, 2000);
            }
          );
      } else {
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Your Application is safe :)'
        });
      }
    });
  }

  pageChanged(nextPageNumber) {
    this.pagingQuery = `page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    this.pageNo = nextPageNumber;
    this.getEventApplications(this.pagingQuery, this.queryFilter);
  }
}
