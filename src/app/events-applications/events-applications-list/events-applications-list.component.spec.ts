import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsApplicationsListComponent } from './events-applications-list.component';

describe('EventsApplicationsListComponent', () => {
  let component: EventsApplicationsListComponent;
  let fixture: ComponentFixture<EventsApplicationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsApplicationsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsApplicationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
