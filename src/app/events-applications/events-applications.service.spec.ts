import { TestBed } from '@angular/core/testing';

import { EventsApplicationsService } from './events-applications.service';

describe('EventsApplicationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventsApplicationsService = TestBed.get(EventsApplicationsService);
    expect(service).toBeTruthy();
  });
});
