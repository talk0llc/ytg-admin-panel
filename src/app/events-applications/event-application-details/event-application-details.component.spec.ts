import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventApplicationDetailsComponent } from './event-application-details.component';

describe('EventApplicationDetailsComponent', () => {
  let component: EventApplicationDetailsComponent;
  let fixture: ComponentFixture<EventApplicationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventApplicationDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventApplicationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
