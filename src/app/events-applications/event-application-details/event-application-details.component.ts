import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EventsApplicationsService } from '../events-applications.service';
import swal from 'sweetalert2';

import {DomSanitizer} from '@angular/platform-browser';
@Component({
  selector: 'app-event-application-details',
  templateUrl: './event-application-details.component.html',
  styleUrls: ['./event-application-details.component.css']
})
export class EventApplicationDetailsComponent implements OnInit {
  public eventId = this.route.snapshot.params.id;
  public applicationId = this.route.snapshot.params.applicationId;
  public loading = false;
  public eventApplicationQA;
  public response = '';
  public grade = '';
  public email = '';
  public name = '';
  public mobile = '';
  form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private eventApplicationDetailsService: EventsApplicationsService,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.getOne(this.eventId, this.applicationId);
    this.buildForm();
  }
  getOne(id, applicationId) {
    this.loading = true;
    this.eventApplicationDetailsService
      .getApplication(id, applicationId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.eventApplicationQA = result.questionAnswers;
          this.email = result.user.email;
          this.name = result.user.first_name;
          this.mobile = result.user.mobile;
          this.response = result.responseNote;
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  update(id, applicationId, status) {
    const data = {
      responseNote: this.form.value.response,
      status
    };
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Update Application?',
      text: 'You are going to update the application status',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, update!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.eventApplicationDetailsService
          .updateApplication(id, applicationId, data)
          .subscribe(
            (result: any) => {
              this.loading = false;
              const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
              this.router.navigate(['events/applications']);
            },
            (error: Response) => {
              this.loading = false;
            }
          );
      } else {
        this.loading = false;
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Keep Status'
        });
      }
    });
  }

  buildForm() {
    this.form = this.fb.group({
      response: [this.response],
    });
  }

}
