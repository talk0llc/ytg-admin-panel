import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './auth/login/login.component';
import {AppLayoutComponent} from './layout/app-layout/app-layout.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ListUsersComponent} from './users/list-users/list-users.component';
import {RegisterComponent} from './auth/register/register.component';
import {ForgetPasswordComponent} from './auth/forget-password/forget-password.component';
import {ResetPasswordComponent} from './auth/reset-password/reset-password.component';
import {CreateUserComponent} from './users/create-user/create-user.component';
import {EditUserComponent} from './users/edit-user/edit-user.component';
import {ProfileComponent} from './staff/profile/profile.component';

import {ListStaffComponent} from './staff/list-staff/list-staff.component';
import {CreateStaffComponent} from './staff/create-staff/create-staff.component';
import {EditStaffComponent} from './staff/edit-staff/edit-staff.component';

import {ListPartnersComponent} from './partners/list-partners/list-partners.component';
import {CreatePartnerComponent} from './partners/create-partner/create-partner.component';
import {EditPartnerComponent} from './partners/edit-partner/edit-partner.component';

import {ListEventsComponent} from './events/list-events/list-events.component';
import {CreateEventComponent} from './events/create-event/create-event.component';
import {EditEventComponent} from './events/edit-event/edit-event.component';
import {CreateEventApplicationComponent} from './events-applications/create-application/create-application.component';
import {EventApplicationDetailsComponent} from './events-applications/event-application-details/event-application-details.component';

import {ListJobsComponent} from './jobs/list-jobs/list-jobs.component';
import {CreateJobComponent} from './jobs/create-job/create-job.component';
import {EditJobComponent} from './jobs/edit-job/edit-job.component';
import {CreateJobApplicationComponent} from './jobs-applications/create-application/create-application.component';
import {JobApplicationDetailsComponent} from './jobs-applications/job-application-details/job-application-details.component';

import {ListProgramsComponent} from './programs/list-programs/list-programs.component';
import {CreateProgramComponent} from './programs/create-program/create-program.component';
import {EditProgramComponent} from './programs/edit-program/edit-program.component';
import {CreateProgramApplicationComponent} from './programs-applications/create-application/create-application.component';
import {ProgramApplicationDetailsComponent} from './programs-applications/program-application-details/program-application-details.component';

import {ListNewsComponent} from './news/list-news/list-news.component';
import {CreateNewsComponent} from './news/create-news/create-news.component';
import {EditNewsComponent} from './news/edit-news/edit-news.component';

import {ListServicesComponent} from './services/list-services/list-services.component';
import {CreateServiceComponent} from './services/create-service/create-service.component';
import {EditServiceComponent} from './services/edit-service/edit-service.component';

import {ContentsComponent} from './contents/contents/contents.component';
import {ListTeamMembersComponent} from './team-members/list-team-members/list-team-members.component';
import {CreateTeamMemberComponent} from './team-members/create-team-member/create-team-member.component';
import {EditTeamMemberComponent} from './team-members/edit-team-member/edit-team-member.component';
import {JobsApplicationsListComponent} from './jobs-applications/jobs-applications-list/jobs-applications-list.component';
import {ProgramsApplicationsListComponent} from './programs-applications/programs-applications-list/programs-applications-list.component';
import {EventsApplicationsListComponent} from './events-applications/events-applications-list/events-applications-list.component';
// { path: 'auth/register', component: RegisterComponent},
import { AuthGuard } from '../app/auth.guard';

const routes: Routes = [
  { path: 'auth/login', component: LoginComponent},
  { path: 'auth/forget-password', component: ForgetPasswordComponent},
  { path: 'auth/reset-password', component: ResetPasswordComponent},
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
  {
    path: '',
    component: AppLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'contents', component: ContentsComponent },
      { path: 'users/list', component: ListUsersComponent },
      { path: 'users/create-user', component: CreateUserComponent },
      { path: 'users/:id/edit-user', component: EditUserComponent },
      { path: 'users', redirectTo: 'users/list', pathMatch: 'full' },
      { path: 'staff/profile', component: ProfileComponent },
      { path: 'staff/list', component: ListStaffComponent },
      { path: 'staff/create-staff', component: CreateStaffComponent },
      { path: 'staff/:id/edit-staff', component: EditStaffComponent },
      { path: 'staff', redirectTo: 'staff/list', pathMatch: 'full' },
      { path: 'team-member/list', component: ListTeamMembersComponent },
      { path: 'team-member/create-team-member', component: CreateTeamMemberComponent },
      { path: 'team-member/:id/edit-team-member', component: EditTeamMemberComponent },
      { path: 'team-member', redirectTo: 'team-member/list', pathMatch: 'full' },
      { path: 'partners/list', component: ListPartnersComponent },
      { path: 'partners/create-partner', component: CreatePartnerComponent },
      { path: 'partners/:id/edit-partner', component: EditPartnerComponent },
      { path: 'partners', redirectTo: 'partners/list', pathMatch: 'full' },
      { path: 'events/list', component: ListEventsComponent },
      { path: 'events/create-event', component: CreateEventComponent },
      { path: 'events/:id/edit-event', component: EditEventComponent },
      { path: 'events/:id/questions', component: CreateEventApplicationComponent },
      { path: 'events/applications', component: EventsApplicationsListComponent },
      { path: 'events/:id/applications/:applicationId', component: EventApplicationDetailsComponent},
      { path: 'events', redirectTo: 'events/list', pathMatch: 'full' },
      { path: 'news/list', component: ListNewsComponent },
      { path: 'news/create-news', component: CreateNewsComponent },
      { path: 'news/:id/edit-news', component: EditNewsComponent },
      { path: 'news', redirectTo: 'news/list', pathMatch: 'full' },
      { path: 'jobs/list', component: ListJobsComponent },
      { path: 'jobs/create-job', component: CreateJobComponent },
      { path: 'jobs/:id/edit-job', component: EditJobComponent },
      { path: 'jobs', redirectTo: 'jobs/list', pathMatch: 'full' },
      { path: 'jobs/:id/questions', component: CreateJobApplicationComponent },
      { path: 'jobs/applications', component: JobsApplicationsListComponent },
      { path: 'jobs/:id/applications/:applicationId', component: JobApplicationDetailsComponent },
      { path: 'services/list', component: ListServicesComponent },
      { path: 'services/create-service', component: CreateServiceComponent },
      { path: 'services/:id/edit-service', component: EditServiceComponent },
      { path: 'services', redirectTo: 'services/list', pathMatch: 'full' },
      { path: 'programs/list', component: ListProgramsComponent },
      { path: 'programs/create-program', component: CreateProgramComponent },
      { path: 'programs/:id/edit-program', component: EditProgramComponent },
      { path: 'programs/:id/questions', component: CreateProgramApplicationComponent },
      { path: 'programs/applications', component: ProgramsApplicationsListComponent },
      { path: 'programs/:id/applications/:applicationId', component: ProgramApplicationDetailsComponent },
      { path: 'programs', redirectTo: 'programs/list', pathMatch: 'full' },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
