import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { NewsService } from '../news.service';
import swal from "sweetalert2";
declare var $;
@Component({
  selector: 'app-create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.css']
})
export class CreateNewsComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private newsService: NewsService,
  ) { }
  get title() {
    return this.form.get('title');
  }

  get description() {
    return this.form.get('description');
  }

  get tags() {
    return this.form.get('tags');
  }

  get segment() {
    return this.form.get('segment');
  }
  public ctrl = this;
  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
  };  public pinned = true;
  public is_active = false;
  public loading = false;
  public segments = [
    { color: 'Red', code: '#ff5a61' },
    { color: 'Blue', code: '#04adc7' },
    { color: 'Orange', code: '#fea601' },
    { color: 'Purple', code: '#8b199b' },
  ];
  form: FormGroup;
  successMsg: string;
  public selectedFiles = [];
  public formData = new FormData();
  errorMsg: string;
  message: string;

  urls = [];

  ngOnInit() {
    this.buildForm();
  }
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        const reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  create() {
    this.loading = true;
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      title: this.form.value.title,
      description: this.form.value.description,
      tags: this.form.value.tags,
      segment: this.form.value.segment,
      pinned: this.pinned,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.newsService
      .create(this.formData)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","New Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.router.navigate(['news']);
          })
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  buildForm() {
    this.form = this.fb.group({
      title: ['', Validators.required],
      description: [''],
      tags: [''],
      segment: [this.segments]
    });
  }

}
