import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { NewsService } from '../news.service';
import swal from 'sweetalert2';

declare var $;
@Component({
  selector: 'app-list-news',
  templateUrl: './list-news.component.html',
  styleUrls: ['./list-news.component.css']
})
export class ListNewsComponent implements OnInit {
  public ctrl = this;
  public loading = false;
  public news = [];
  public nameFilter = '';
  successMsg: string;
  errorMsg: string;
  public query = '';
  pageNo: number = 1;
  limit: number = 5;
  totalPages: number;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private newsService: NewsService,
  ) { }

  ngOnInit() {
    this.query = `page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    this.getAll(this.query);
  }

  filter (value) {
    if (value !== '') {
      this.query = `title=${value}&page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    } else {
      this.query = `page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    }
    this.getAll(this.query);
  }

  getAll(query) {
    this.loading = true;
    this.newsService
      .getAll(query)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.news = result.news;
          this.totalPages = result.totalCount;
          // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          // this.router.navigate(['news']);
          // setTimeout(() => {
          //   this.successMsg = 'News Created Successfully';
          // }, 2000);
        },
        (error: Response) => {
          this.loading = false;
          // setTimeout(() => {
          //   this.errorMsg = error;
          // }, 2000);
        }
      );
  }

  delete (id) {
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Delete News?',
      text: 'You will not be able to recover the data of News',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.newsService
          .delete(id)
          .subscribe(
            (result: any) => {
              this.loading = false;
              this.getAll(this.query);
              // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
              // this.router.navigate(['news']);
              // setTimeout(() => {
              //   this.successMsg = 'News Created Successfully';
              // }, 2000);
            },
            (error: Response) => {
              this.loading = false;
              this.getAll(this.query);
              // setTimeout(() => {
              //   this.errorMsg = error;
              // }, 2000);
            }
          );
      } else {
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Your News file is safe :)'
        });
      }
    });
  }

  pageChanged(nextPageNumber) {
    if (this.nameFilter !== '') {
      this.query = `title=${this.nameFilter}&page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    } else {
      this.query = `page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    }
    this.pageNo = nextPageNumber;
    this.getAll(this.query);
  }

}
