import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { StaffService } from '../staff.service';
import {isEmpty} from 'rxjs/operators';
import swal from "sweetalert2";
declare var $;

@Component({
  selector: 'app-create-staff',
  templateUrl: './create-staff.component.html',
  styleUrls: ['./create-staff.component.css']
})
export class CreateStaffComponent implements OnInit {
  public ctrl = this;
  public resource = {
    resource: '',
    accessType: ['createAny', 'updateAny', 'deleteAny', 'readAny']
  };
  public resources = [];
  public rolesList = [
    { key: 'Programs', value: 'programs', checked: false},
    { key: 'News', value: 'news', checked: false},
    { key: 'Jobs', value: 'jobs', checked: false},
    { key: 'Events', value: 'events', checked: false},
    { key: 'Staff', value: 'employees', checked: false},
    { key: 'Users', value: 'users', checked: false},
    { key: 'Services', value: 'services', checked: false},
    { key: 'Partners', value: 'partners', checked: false},
    { key: 'Team Members', value: 'members', checked: false},
    { key: 'Contents', value: 'contents', checked: false},
    { key: 'Applications', value: 'applications', checked: false},
    ];
  public genders = ['Male', 'Female'];
  public types = ['staff', 'team'];
  public pinned = false;
  public is_active = false;
  public loading = false;
  public selectedFiles = [];
  public formData = new FormData();
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private staffService: StaffService,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  get email() {
    return this.form.get('email');
  }
    get password() {
    return this.form.get('password');
  }
    get first_name() {
    return this.form.get('first_name');
  }
    get last_name() {
    return this.form.get('last_name');
  }
    get mobile() {
    return this.form.get('mobile');
  }
    get role() {
    return this.form.get('role');
  }
    get gender() {
    return this.form.get('gender');
  }

  get type() {
    return this.form.get('type');
  }
    get linkedinUrl() {
    return this.form.get('linkedinUrl');
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }
  create() {
    for (let i = 0; i < this.rolesList.length; i += 1) {
      if (this.rolesList[i].checked === true) {
        this.resource = {
          resource: this.rolesList[i].value,
          accessType: ['deleteAny', 'readAny', 'createAny', 'updateAny']
        };
        this.resources.push(this.resource);
      }
    }
    const roles = [];
    roles.push({
      name: 'staff',
      resources: this.resources
    });
    this.loading = true;
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      email: this.form.value.email,
      password: this.form.value.password,
      first_name: this.form.value.first_name,
      last_name: this.form.value.last_name,
      mobile: this.form.value.mobile,
      linkedinUrl: this.form.value.linkedinUrl,
      roles: roles,
      gender: this.form.value.gender,
      type: this.form.value.type,
      pinned: this.pinned,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.staffService
      .create(this.formData)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","Staff Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.router.navigate(['staff']);
          })
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }
  buildForm() {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      mobile: ['', Validators.required],
      gender: [this.genders],
      type: [this.types],
      role: [this.rolesList],
      linkedinUrl: [''],
    });
  }
}
