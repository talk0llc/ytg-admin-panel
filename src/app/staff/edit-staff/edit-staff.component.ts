import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { StaffService } from '../staff.service';
import {environment} from '../../../environments/environment';
declare var $;
@Component({
  selector: 'app-edit-staff',
  templateUrl: './edit-staff.component.html',
  styleUrls: ['./edit-staff.component.css']
})
export class EditStaffComponent implements OnInit {
  public staffId = this.route.snapshot.params.id;
  public email =  '';
  public password =  '';
  public first_name =  '';
  public last_name =  '';
  public mobile =  '';
  public linkedinUrl =  '';
  public  role =  '';
  public gender =  '';
  public type =  '';
  public image =  [];
  public imagesUrl = [];
  public resource = {
    resource: '',
    accessType: ['createAny', 'updateAny', 'deleteAny', 'readAny']
  };
  public resources = [];
  public roles = [
    { key: 'Programs', value: 'programs', checked: false},
    { key: 'News', value: 'news', checked: false},
    { key: 'Jobs', value: 'jobs', checked: false},
    { key: 'Events', value: 'events', checked: false},
    { key: 'Staff', value: 'employees', checked: false},
    { key: 'Users', value: 'users', checked: false},
    { key: 'Services', value: 'services', checked: false},
    { key: 'Partners', value: 'partners', checked: false},
    { key: 'Team Members', value: 'members', checked: false},
    { key: 'Contents', value: 'contents', checked: false},
    { key: 'Applications', value: 'applications', checked: false},
  ];
  public genders = ['Male', 'Female'];
  public types = ['staff', 'team'];
  public pinned = false;
  public is_active = false;
  public loading = false;
  public selectedFiles = [];
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  public formData = new FormData();
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private staffService: StaffService,
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getOne(this.staffId);
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  update() {
    for (let i = 0; i < this.roles.length; i += 1) {
      if (this.roles[i].checked === true) {
        this.resource = {
          resource: this.roles[i].value,
          accessType: ['deleteAny', 'readAny', 'createAny', 'updateAny']
        };
        this.resources.push(this.resource);
      }
    }
    const roles = [];
    roles.push({
      name: 'staff',
      resources: this.resources
    });
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      email: this.form.value.email,
      first_name: this.form.value.first_name,
      last_name: this.form.value.last_name,
      mobile: this.form.value.mobile,
      linkedinUrl: this.form.value.linkedinUrl,
      roles: roles,
      gender: this.form.value.gender,
      types: this.form.value.type,
      pinned: this.pinned,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.loading = true;
    this.staffService
      .update(this.formData, this.staffId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.router.navigate(['staff']);
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  getOne(id) {
    this.loading = true;
    this.staffService
      .getOne(id)
      .subscribe(
        (result: any) => {
          for (let i = 0; i < result.roles[0].resources.length; i += 1) {
            for (let j = 0; j < this.roles.length; j += 1) {
              if (result.roles[0].resources[i].resource === this.roles[j].value) {
                this.roles[j].checked = true;
              }
            }
          }
          this.email =  result.email;
          this.password =  result.password;
          this.first_name =  result.first_name;
          this.last_name =  result.last_name;
          this.mobile =  result.mobile;
          this.linkedinUrl =  result.linkedinUrl;
          this.gender =  result.gender;
          this.type =  result.type;
          for (let i = 0; i < result.images.length; i += 1) {
            this.imagesUrl.push(`${ environment.mainIpUrl }/${ result.images[i] }`);
          }
          // this.urls1 = ['http://209.97.176.62/images/uploads/1542979416903.jpg'];
          this.urls = this.imagesUrl;
          this.pinned =  result.pinned;
          this. is_active =  result.is_active;
          this.loading = false;
          this.imagesUrl = [];
          this.buildForm();
        },
        (error: Response) => {
          this.loading = false;
        }
      );
  }

  buildForm() {
    this.form = this.fb.group({
      email: [this.email, Validators.required],
      password: [this.password, Validators.required],
      first_name: [this.first_name, Validators.required],
      last_name: [this.last_name, Validators.required],
      mobile: [this.mobile, Validators.required],
      gender: [this.gender],
      type: [this.type],
      role: [this.roles],
      linkedinUrl: [this.linkedinUrl],
    });
  }
}
