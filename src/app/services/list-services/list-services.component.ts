import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { ServicesService } from '../services.service';
import swal from 'sweetalert2';

declare var $;
@Component({
  selector: 'app-list-services',
  templateUrl: './list-services.component.html',
  styleUrls: ['./list-services.component.css']
})
export class ListServicesComponent implements OnInit {
  public ctrl = this;
  public loading = false;
  public services = [];
  public nameFilter = '';
  successMsg: string;
  errorMsg: string;
  public query = '';
  pageNo: number = 1;
  limit: number = 5;
  totalPages: number;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private serviceService: ServicesService,
  ) { }

  ngOnInit() {
    this.query = `page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    this.getAll(this.query);
  }

  filter (value) {
    if (value !== '') {
      this.query = `name=${value}&page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    } else {
      this.query = `page=${ this.pageNo - 1 }&limit=${ this.limit }`;
    }
    this.getAll(this.query);
  }

  getAll(query) {
    this.loading = true;
    this.serviceService
      .getAll(query)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.services = result.services;
          this.totalPages = result.totalCount;
          // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          // this.router.navigate(['news']);
          // setTimeout(() => {
          //   this.successMsg = 'News Created Successfully';
          // }, 2000);
        },
        (error: Response) => {
          this.loading = false;
          // setTimeout(() => {
          //   this.errorMsg = error;
          // }, 2000);
        }
      );
  }

  delete (id) {
    this.loading = true;
    swal({
      type: 'warning',
      title: 'Are you sure to Delete Service?',
      text: 'You will not be able to recover the data of Service',
      showCancelButton: true,
      confirmButtonColor: '#049F0C',
      cancelButtonColor: '#ff0000',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.serviceService
          .delete(id)
          .subscribe(
            (result: any) => {
              this.loading = false;
              this.getAll(this.query);
              // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
              // this.router.navigate(['news']);
              // setTimeout(() => {
              //   this.successMsg = 'News Created Successfully';
              // }, 2000);
            },
            (error: Response) => {
              this.loading = false;
              this.getAll(this.query);
              // setTimeout(() => {
              //   this.errorMsg = error;
              // }, 2000);
            }
          );
      } else {
        swal({
          type: 'info',
          title: 'Cancelled',
          text: 'Your Service file is safe :)'
        });
      }
    });
  }
  pageChanged(nextPageNumber) {
    if (this.nameFilter !== '') {
      this.query = `name=${this.nameFilter}&page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    } else {
      this.query = `page=${ nextPageNumber - 1 }&limit=${ this.limit }`;
    }
    this.pageNo = nextPageNumber;
    this.getAll(this.query);
  }

}
