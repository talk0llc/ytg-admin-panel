import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  resMail: string;
  constructor(private fb: FormBuilder, private authService: AuthService) { }

  ngOnInit() {
    this.buildForm();
  }

  public get email() {
    return this.form.get('email');
  }

  buildForm() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  forgetPassword() {
    this.authService.forgetPassword(this.form.value.email).subscribe(
      (res: any) => {
        this.resMail = res.data.email;
        setTimeout(() => {
          this.successMsg = '';
        }, 3000);
      },
      err => {
        setTimeout(() => {
          this.errorMsg = '';
        }, 3000);
      }
    );
    this.form.reset();
  }
}
