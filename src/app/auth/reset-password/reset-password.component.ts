import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PASSWORD_REGEX } from '../../constants';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  form: FormGroup;
  errorMsg: string;
  successMsg: string;
  code: string;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.code = params.code;
    });
    this.buildForm();
  }

  public get password() {
    return this.form.get('password');
  }

  public get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  buildForm() {
    this.form = this.fb.group({
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(PASSWORD_REGEX)
        ]
      ],
      confirmPassword: ['', [Validators.required, this.passwordConfirming]]
    });
  }

  passwordConfirming(c: AbstractControl): any {
    if (!c.parent || !c) {
      return;
    }
    const pwd = c.parent.get('password');
    const cpwd = c.parent.get('confirmPassword');

    if (!pwd || !cpwd) {
      return;
    }
    if (pwd.value !== cpwd.value) {
      return { invalid: true };
    }
  }

  resetPassword() {
    this.authService
      .resetPassword(this.code, this.form.value.password)
      .subscribe(
        res => {
          setTimeout(() => {
            this.successMsg = '';
            this.router.navigate(['login']);
          }, 3000);
        },
        err => {
          setTimeout(() => {
            this.errorMsg = '';
          }, 3000);
        }
      );
    this.form.reset();
  }

}
