import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { PASSWORD_REGEX, PHONE_REGEX } from '../../constants';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public ctrl = this;
  public loading = false;
  form: FormGroup;
  errorMsg;
  successMsg: string;
  myModel = [];
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  get email() {
    return this.form.get('email');
  }

  get mobileNumber() {
    return this.form.get('mobileNumber');
  }

  get password() {
    return this.form.get('password');
  }

  signUp() {
    this.loading = true;
    const data = {
      first_name: this.form.value.firstName,
      last_name: this.form.value.lastName,
      email: this.form.value.email,
      mobile: this.form.value.mobileNumber,
      password: this.form.value.password
    };
    this.authService.signUp(data).subscribe(
      (res: any) => {
        this.loading = false;
        this.successMsg = 'Success!, Your account created';
        setTimeout(() => {
          this.successMsg = '';
          this.router.navigate(['dashboard']);
        }, 2000);
        this.saveUserData(res.data._id, res.data.token, res);
      },
      (error: Response) => {
        this.loading = false;
        setTimeout(() => {
          this.errorMsg = '';
        }, 2000);
      }
    );
  }

  saveUserData(id: string, token: string, user:any) {
    this.authService.saveUserData(id, token, user);
  }

  buildForm() {
    this.form = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobileNumber: ['', [Validators.required, Validators.pattern(PHONE_REGEX)]],
      password: ['', [Validators.required, Validators.pattern(PASSWORD_REGEX)]]
    });
  }
}
