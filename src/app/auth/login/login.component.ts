import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import swal from "sweetalert2";

declare var $;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public ctrl = this;
  public loading = false;
  form: FormGroup;
  message: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
  ) {}


  ngOnInit() {
    if (this.authService.isLoggednIn()) {
      this.router.navigate(['/dashboard']);
    }
    this.buildForm();
    document.body.className = 'hold-transition login-page';

    $(() => {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%' /* optional */
      });
    });
  }
  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }
  login() {
    // this.router.navigate(['dashboard']);
    this.loading = true;
    this.authService
      .login(this.form.value.email, this.form.value.password, 'employee')
      .subscribe(
        (result: any) => {
          this.loading = false;
          const id = result._id;
          const token = result.token;
          this.authService.saveUserData(id, token, result);
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.router.navigate([returnUrl || 'dashboard']);
          this.message = '';
        },
        (error: Response) => {
          swal({
            type: 'warning',
            title: 'Try again',
            text: 'Email or password are not correct.',
            confirmButtonText: 'Ok!',
          });
          this.loading = false;
        }
      );
  }
  buildForm() {
    this.form = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    });
  }
}
