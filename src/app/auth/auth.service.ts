import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AUTH_TOKEN, USER_ID} from '../constants';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl = environment.apiBaseUrl;
  private userId: string = null;
  private _isAuthenticated = new BehaviorSubject(false);
  constructor(private http: HttpClient) { }

  // Providing a observable to listen the authentication state
  get isAuthenticated(): Observable<boolean> {
    return this._isAuthenticated.asObservable();
  }
  setUserId(id: string) {
    this.userId = id;

    // Dispatching to all listeners that the user is authenticated
    this._isAuthenticated.next(true);
  }

  saveUserData(id: string, token: string, user:any) {
    localStorage.setItem(USER_ID, id);
    localStorage.setItem(AUTH_TOKEN, token);
    localStorage.setItem('fname', user.first_name);
    localStorage.setItem('lname', user.last_name);
    localStorage.setItem('images', user.images);
    this.setUserId(id);
  }

  forgetPassword(email) {
    return this.http.post(`${ this.apiUrl }/forget_password`, { email });
  }

  resetPassword(code, password) {
    return this.http.post(`${ this.apiUrl }/reset_password`, { code, password });
  }

  login(username, password, type) {
    return this.http.post(`${ this.apiUrl }/login`, { username, password, type });
  }

  signUp(data) {
    return this.http.post(`${ this.apiUrl }/register`, data);
  }

  logout() {
    // Removing user data from local storage and the service
    localStorage.removeItem(USER_ID);
    localStorage.removeItem(AUTH_TOKEN);
    this.userId = null;

    // Dispatching to all listeners that the user is not authenticated
    this._isAuthenticated.next(false);
  }

  autoLogin() {
    const id = localStorage.getItem(USER_ID);

    if (id) {
      this.setUserId(id);
    }
  }

  getToken() {
    return localStorage.getItem(AUTH_TOKEN);
  }

  isLoggednIn() {
    return this.getToken() !== null;
  }
}
