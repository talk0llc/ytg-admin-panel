import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './auth/login/login.component';
import {ListUsersComponent} from './users/list-users/list-users.component';
import {TopnavbarComponent} from './layout/topnavbar/topnavbar.component';
import {SidenavbarComponent} from './layout/sidenavbar/sidenavbar.component';
import {FooternavbarComponent} from './layout/footernavbar/footernavbar.component';
import {SettingsnavbarComponent} from './layout/settingsnavbar/settingsnavbar.component';
import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import { RegisterComponent } from './auth/register/register.component';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';
import { CreateUserComponent } from './users/create-user/create-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { ProfileComponent } from './staff/profile/profile.component';
import { ListStaffComponent } from './staff/list-staff/list-staff.component';
import { CreateStaffComponent } from './staff/create-staff/create-staff.component';
import { EditStaffComponent } from './staff/edit-staff/edit-staff.component';
import { ListJobsComponent } from './jobs/list-jobs/list-jobs.component';
import { CreateJobComponent } from './jobs/create-job/create-job.component';
import { EditJobComponent } from './jobs/edit-job/edit-job.component';
import { ListNewsComponent } from './news/list-news/list-news.component';
import { CreateNewsComponent } from './news/create-news/create-news.component';
import { EditNewsComponent } from './news/edit-news/edit-news.component';
import { ListEventsComponent } from './events/list-events/list-events.component';
import { CreateEventComponent } from './events/create-event/create-event.component';
import { EditEventComponent } from './events/edit-event/edit-event.component';
import { ListServicesComponent } from './services/list-services/list-services.component';
import { CreateServiceComponent } from './services/create-service/create-service.component';
import { EditServiceComponent } from './services/edit-service/edit-service.component';
import { ListProgramsComponent } from './programs/list-programs/list-programs.component';
import { CreateProgramComponent } from './programs/create-program/create-program.component';
import { EditProgramComponent } from './programs/edit-program/edit-program.component';
import { CreateProgramApplicationComponent } from './programs-applications/create-application/create-application.component';
import { ContentsComponent } from './contents/contents/contents.component';
import { CreatePartnerComponent } from './partners/create-partner/create-partner.component';
import { EditPartnerComponent } from './partners/edit-partner/edit-partner.component';
import { ListPartnersComponent } from './partners/list-partners/list-partners.component';
import { CreateEventApplicationComponent } from './events-applications/create-application/create-application.component';
import { CreateTeamMemberComponent } from './team-members/create-team-member/create-team-member.component';
import { EditTeamMemberComponent } from './team-members/edit-team-member/edit-team-member.component';
import { ListTeamMembersComponent } from './team-members/list-team-members/list-team-members.component';
import { CreateJobApplicationComponent } from './jobs-applications/create-application/create-application.component';
import { ProgramApplicationDetailsComponent } from './programs-applications/program-application-details/program-application-details.component';
import { EventApplicationDetailsComponent } from './events-applications/event-application-details/event-application-details.component';
import { JobApplicationDetailsComponent } from './jobs-applications/job-application-details/job-application-details.component';
import { EventsApplicationsListComponent } from './events-applications/events-applications-list/events-applications-list.component';
import { JobsApplicationsListComponent } from './jobs-applications/jobs-applications-list/jobs-applications-list.component';
import { ProgramsApplicationsListComponent } from './programs-applications/programs-applications-list/programs-applications-list.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    ListUsersComponent,
    TopnavbarComponent,
    SidenavbarComponent,
    FooternavbarComponent,
    SettingsnavbarComponent,
    AppLayoutComponent,
    RegisterComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    CreateUserComponent,
    EditUserComponent,
    ProfileComponent,
    ListStaffComponent,
    CreateStaffComponent,
    EditStaffComponent,
    ListJobsComponent,
    CreateJobComponent,
    EditJobComponent,
    ListNewsComponent,
    CreateNewsComponent,
    EditNewsComponent,
    ListEventsComponent,
    CreateEventComponent,
    EditEventComponent,
    ListServicesComponent,
    CreateServiceComponent,
    EditServiceComponent,
    ListProgramsComponent,
    CreateProgramComponent,
    EditProgramComponent,
    CreateProgramApplicationComponent,
    ContentsComponent,
    CreatePartnerComponent,
    EditPartnerComponent,
    ListPartnersComponent,
    CreateEventApplicationComponent,
    CreateTeamMemberComponent,
    EditTeamMemberComponent,
    ListTeamMembersComponent,
    CreateJobApplicationComponent,
    ProgramApplicationDetailsComponent,
    EventApplicationDetailsComponent,
    JobApplicationDetailsComponent,
    EventsApplicationsListComponent,
    JobsApplicationsListComponent,
    ProgramsApplicationsListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AngularEditorModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
