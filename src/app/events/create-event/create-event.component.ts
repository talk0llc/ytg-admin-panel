import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EventsService } from '../events.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import swal from "sweetalert2";

declare var $: any;
@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  public ctrl = this;
  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
  };
  public pinned = false;
  public is_active = false;
  public loading = false;
  public segments = [
    { color: 'Red', code: '#ff5a61' },
    { color: 'Blue', code: '#04adc7' },
    { color: 'Orange', code: '#fea601' },
    { color: 'Purple', code: '#8b199b' },
  ];
  public selectedFiles = [];
  public formData = new FormData();
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private eventsService: EventsService,
  ) { }

  ngOnInit() {
    $(function () {
      // Date picker
      $('#datepicker').datepicker({
        autoclose: true
      });
    });
    this.buildForm();
  }
  get title() {
    return this.form.get('title');
  }

  get description() {
    return this.form.get('description');
  }

  get location() {
    return this.form.get('location');
  }

  get dueDate() {
    return this.form.get('dueDate');
  }

  get tags() {
    return this.form.get('tags');
  }

  get segment() {
    return this.form.get('segment');
  }

  urls = [];
  onFileChanged(event) {
    this.selectedFiles = event.target.files;
    // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        let reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          this.urls.push(event.target.result);
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  create() {
    this.loading = true;
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      title: this.form.value.title,
      description: this.form.value.description,
      tags: this.form.value.tags,
      segment: this.form.value.segment,
      location: this.form.value.location,
      dueDate: this.form.value.dueDate,
      pinned: this.pinned,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.eventsService
      .create(this.formData)
      .subscribe(
        (result: any) => {
          this.loading = false;
          swal("","Event Created Successfully", "success")
          .then((value) => {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.router.navigate(['events']);
          })
          // setTimeout(() => {
          //   this.successMsg = 'News Created Successfully';
          // }, 2000);
        },
        (error: Response) => {
          this.loading = false;
          // setTimeout(() => {
          //   this.errorMsg = error;
          // }, 2000);
        }
      );
  }

  buildForm() {
    this.form = this.fb.group({
      title: ['', Validators.required],
      description: [''],
      segment: [this.segments],
      tags: [''],
      location: [''],
      dueDate: [''],
    });
  }

}
