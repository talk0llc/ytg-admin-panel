import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EventsService } from '../events.service';
import { environment } from '../../../environments/environment';
import {AngularEditorConfig} from '@kolkov/angular-editor';
declare var $: any;

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private eventsService: EventsService,
  ) { }
  get section() {
    return this.form.get('section');
  }
  public ctrl = this;
  public eventId = this.route.snapshot.params.id;
  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: `/app/images/uploads`, // if needed
  };
  public mainIpUrl = environment.mainIpUrl;
  public editorConfig2: AngularEditorConfig = {
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: `/app/images/uploads`, // if needed
  };
  public pinned = true;
  public title = '';
  public description = '';
  public tags = '';
  public segment = '';
  public segments = [
    { color: 'Red', code: '#ff5a61' },
    { color: 'Blue', code: '#04adc7' },
    { color: 'Orange', code: '#fea601' },
    { color: 'Purple', code: '#8b199b' },
  ];
  public sections = ['Text', 'File'];
  public location = '';
  public dueDate: string;
  public eventDate: string;
  public is_active = false;
  public loading = false;
  public selectedFiles = [];
  public imagesUrl = [];
  public extraSections = [];
  form: FormGroup;
  successMsg: string;
  errorMsg: string;
  message: string;
  public formData = new FormData();

  urls1 = [];
  urls2 = [];
  ngOnInit() {
    $(function () {
      // Date picker
      $('#datepicker').datepicker({
        autoclose: true,
        onSelect: function(dateText) {
        }
      }).on('change', function() {
        return this.value;
      });
    });
    this.buildForm();
    this.getOne(this.eventId);
  }
  onFileChanged(event, no) {
    this.selectedFiles = event.target.files;
    if (this.selectedFiles.length > 0) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        const reader = new FileReader();

        reader.onload = (event: any) => {
          this.formData.append('images', this.selectedFiles[i]);
          if (no === 1) {
            this.urls1.push(event.target.result);
          } else {
            this.urls2.push(event.target.result);
          }
        };
        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }

  }

  update() {
    $('.content').find('*').attr('disabled', 'disabled');
    $('.content').find('a').click(function (e) { e.preventDefault(); });
    const data = {
      title: this.form.value.title,
      description: this.form.value.description,
      tags: this.form.value.tags,
      segment: this.form.value.segment,
      location: this.form.value.location,
      dueDate: this.dueDate,
      pinned: this.pinned,
      is_active: this.is_active
    };
    this.formData.append('data', JSON.stringify(data));
    this.loading = true;
    this.eventsService
      .update(this.formData, this.eventId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.formData = new FormData();
          this.router.navigate(['events']);
        },
        (error: Response) => {
          this.loading = false;
          this.formData = new FormData();
        }
      );
  }

  formatDate(date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) { month = '0' + month; }
    if (day.length < 2) { day = '0' + day; }

    return [year, month, day].join('-');
  }
  getOne(id) {
    this.loading = true;
    this.urls1 = [];
    this.eventsService
      .getOne(id)
      .subscribe(
        (result: any) => {
          this.loading = false;
          this.title = result.title;
          this.description = result.description;
          this.tags = result.tags;
          this.segment = result.segment;
          this.location = result.location;
          this.dueDate = this.formatDate(result.dueDate);
          this.extraSections = result.extraSections;
          for (let i = 0; i < result.images.length; i += 1) {
            this.imagesUrl.push(`${ environment.mainIpUrl }/${ result.images[i] }`);
          }
          // this.urls1 = ['http://209.97.176.62/images/uploads/1542979416903.jpg'];
          this.urls1 = this.imagesUrl;
          this.pinned = result.pinned;
          this.is_active = result.is_active;
          this.imagesUrl = [];
          this.buildForm();
        },
        (error: Response) => {
          this.loading = false;
          this.imagesUrl = [];
        }
      );
  }

  updateSections() {
    const data = {
      extraSections: {
        // images: this.urls,
        description: this.form.value.extraDescription
      }
    };
    this.formData.append('data', JSON.stringify(data));
    this.loading = true;
    this.eventsService
      .update(this.formData, this.eventId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.urls2 = [];
          this.form.controls['extraDescription'].setValue('');
          this.formData = new FormData();
          this.getOne(this.eventId);
          this.reset();
        },
        (error: Response) => {
          this.loading = false;
          this.formData = new FormData();
          this.getOne(this.eventId);
          this.reset();
        }
      );
  }

  onSelectType(type) {
    const description = document.getElementById('description');
    const file = document.getElementById('fileupload');
    const saveButton = document.getElementById('saveButton');
    if (type.value === 'Text') {
      description.removeAttribute('hidden');
      file.setAttribute('hidden', 'hidden');
    } else {
      file.removeAttribute('hidden');
      description.setAttribute('hidden', 'hidden');
    }
    saveButton.removeAttribute('disabled');
  }

  reset() {
    const description = document.getElementById('description');
    const file = document.getElementById('fileupload');
    const saveButton = document.getElementById('saveButton');
    file.setAttribute('hidden', 'hidden');
    description.setAttribute('hidden', 'hidden');
    saveButton.setAttribute('disabled', 'disabled');
  }

  deleteSection(sectionId) {
    this.loading = true;
    this.eventsService
      .deleteSection(this.eventId, sectionId)
      .subscribe(
        (result: any) => {
          this.loading = false;
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.getOne(this.eventId);
        },
        (error: Response) => {
          this.loading = false;
          this.getOne(this.eventId);
        }
      );
  }

  buildForm() {
    this.form = this.fb.group({
      title: [this.title, Validators.required],
      description: [this.description, Validators.required],
      tags: [this.tags],
      segment: [this.segment],
      section: [this.sections],
      location: [this.location, Validators.required],
      dueDate: [this.dueDate, Validators.required],
      is_active: [this.is_active],
      pinned: [this.pinned],
      extraDescription: ['']
    });
  }

}
